//////////////////////////
///CSE2 Arithmetic HW02
///Jeanine Minnick Section 211 2/5/18
//compute cost plus tax of purchases
//

public class Arithmetic {
  //main method
  public static void main(String[] args) {
    //input data
    int numPants = 3; //number of pants
    double pantsPrice = 34.98; //price per pair of pants
    int numShirts = 2; //number of sweatshirts
    double shirtPrice = 24.99; //price per sweatshirt 
    int numBelts = 1; //number of belts
    double beltCost = 33.99; //price per belt
    double paSalesTax = 0.06; //the tax rate
    
    double pantsTotal = numPants * pantsPrice; //total cost of pants
    double shirtsTotal = numShirts * shirtPrice; //total cost of shirts
    double beltsTotal = numBelts * beltCost; //total cost of belts
    double total = pantsTotal + shirtsTotal + beltsTotal; //total cost of all clothes
    
    double firstPantsTax = (pantsTotal * paSalesTax) * 100; //pants sales tax
    double pantsTax = ((int) firstPantsTax) / 100.0; //pants sales tax rounded to 2 decimals
    double firstShirtsTax = (shirtsTotal * paSalesTax) * 100; //shirts sales tax
    double shirtsTax = ((int) firstShirtsTax) / 100.0; //shirts sales tax rounded to 2 decimals
    double firstBeltsTax = (beltsTotal * paSalesTax) * 100; //belts sales tax
    double beltsTax = ((int) firstBeltsTax) / 100.0; //belts sales tax rounded to 2 decimals
    double firstTotalTax = (total * paSalesTax) * 100; //total sales tax
    double totalTax = ((int) firstTotalTax) / 100.0; //total sales tax rounded to 2 decimals
    
    double purchaseTotal = totalTax + total; //entire purchase total

    //print out the output data
    System.out.println("The total cost of pants is $" + pantsTotal); // $104.94
    System.out.println("The total cost of sweatshirts is $" + shirtsTotal); // $49.98
    System.out.println("The total cost of belts is $" + beltsTotal); // $33.99
    
    System.out.println("The total sales tax on pants is $" + pantsTax); // $6.29
    System.out.println("The total sales tax on shirts is $" + shirtsTax); // $2.99
    System.out.println("The total sales tax on belts is $" + beltsTax); // $2.03
    
    System.out.println("The total before tax is $" + total); // $188.91
    System.out.println("The total tax is $" + totalTax); // $11.33
    System.out.println("The entire purchase total is $" + purchaseTotal); // $200.24
                       
                       
  } //end of main method
} //end of class



