///////////////
/// Jeanine Minnick lab07
/// Sentences 3/23/18
///// write a program that creates a sentence with multiple methods


import java.util.Scanner;
import java.util.Random;


public class Sentences {
  public static String Adjectives(){
    Random randInt = new Random();
    int random = randInt.nextInt(10);
    String adj = "adj";
    switch(random){
      case 0: adj = "rude"; 
        break;
      case 1: adj = "quick"; 
        break;
      case 2: adj = "fast"; 
        break;
      case 3: adj = "slow"; 
        break;
      case 4: adj = "fat"; 
        break;
      case 5: adj = "mean"; 
        break;
      case 6: adj = "hungry"; 
        break;
      case 7: adj = "boring"; 
        break;
      case 8: adj = "tired"; 
        break;
      case 9: adj = "dirty"; 
        break;
    }
    return adj;
  }
  public static String Nouns(){
    Random randInt = new Random();
    int random = randInt.nextInt(10);
    String noun = "noun";
    switch(random){
      case 1: noun = "tree"; 
        break;
      case 2: noun = "goat"; 
        break;
      case 3: noun = "cow"; 
        break;
      case 4: noun = "sheep"; 
        break;
      case 5: noun = "chair"; 
        break;
      case 6: noun = "bed"; 
        break;
      case 7: noun = "cat"; 
        break;
      case 8: noun = "goose"; 
        break;
      case 9: noun = "ball"; 
        break;
    }
    return noun;
  }
  public static String Verbs(){
    Random randInt = new Random();
    int random = randInt.nextInt(10);
    String verb = "verb";
    switch(random){
      case 1: verb = "ran"; 
        break;
      case 2: verb = "jumped"; 
        break;
      case 3: verb = "rolled"; 
        break;
      case 4: verb = "ate"; 
        break;
      case 5: verb = "sneezed"; 
        break;
      case 6: verb = "cried"; 
        break;
      case 7: verb = "broke"; 
        break;
      case 8: verb = "painted"; 
        break;
      case 9: verb = "slept"; 
        break;
    }
    return verb;
  }
  public static String Noun2(){
   Random randInt = new Random();
    int random = randInt.nextInt(10);
    String noun2 = "noun2";
    switch(random){
      case 0: noun2 = "dog"; 
        break;
      case 1: noun2 = "fox"; 
        break;
      case 2: noun2 = "child"; 
        break;
      case 3: noun2 = "horse"; 
        break;
      case 4: noun2 = "bird"; 
        break;
      case 5: noun2 = "table"; 
        break;
      case 6: noun2 = "pants"; 
        break;
      case 7: noun2 = "witch"; 
        break;
      case 8: noun2 = "frog"; 
        break;
      case 9: noun2 = "dolphin"; 
        break;
    }
    return noun2;
  }
  
  public static String Sentence1() {
    Scanner scan = new Scanner(System.in);
    int cont = 1;
    String adj = " ";
    String noun = " ";
    String verb = " ";
    String noun2 = " ";
    String adj2 = " ";
    String adj3 = " ";
    for(int i=1; i>0; i++){
      if(cont == 1){
        adj = Adjectives();
        noun = Nouns();
        verb = Verbs();
        noun2 = Noun2();
        adj2 = Adjectives();
        adj3 = Adjectives();
        System.out.println("The " + adj + " " + adj2 + " " + noun + " " + verb + " the " + adj3 + " " + noun2);
      }
      else{break;}
      
      System.out.println("Do you want to generate a new sentence (type 1 for yes, any other number for no)?");
      cont = scan.nextInt();
      
    }
    return noun;
  }
  
  public static String Sentence2(String nounStr) {
    String adj = Adjectives();
    String verb = Verbs();
    String noun2 = Noun2();
    String noun3 = Nouns();
    String noun4 = Noun2();
    String adj2 = Adjectives();
    String adj3 = Adjectives();
    
    System.out.println(nounStr + " used " + noun2 + " to " + verb + " " + noun3 + " at the " + adj2 + " " + noun4);
    return nounStr;
  }
  
  public static String Sentence3(String nounStr) {
    String adj = Adjectives();
    String verb = Verbs();
    String noun2 = Noun2();
    String noun3 = Nouns();
    String noun4 = Noun2();
    String adj2 = Adjectives();
    String adj3 = Adjectives();
    
    System.out.println("That " + nounStr + " " + verb + " it's " + noun2 + "!");
    return nounStr;
  }
  
  public static void main(String[] args){
    String noun = Sentence1();
    Sentence2(noun);
    Sentence3(noun);
    
  }
}