//////////////////////////
///CSE 2 Welcome Class
///
public class WelcomeClass {
  
  public static void main(String[] args) {
    ///print welcome box, Lehigh ID, and autobiography.
    System.out.println("  -----------\n  | WELCOME |\n  -----------\n  ^  ^  ^  ^  ^  ^\n / \\/ \\/ \\/ \\/ \\/ \\\n<-J--P--M--2--2--0->\n \\ /\\ /\\ /\\ /\\ /\\ /\n  v  v  v  v  v  v\n I am Jeanine Minnick, I am from Forest, VA. My career goal is to work for a professional sports team doing data analytics, so my major is Statistics.");
  }
}