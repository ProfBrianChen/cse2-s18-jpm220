//////////////////////////////////
/// Jeanine Minnick  hw09 DrawPoker
/// 4/15/18  Section 211
///


import java.util.Random;
import java.util.*;


public class DrawPoker {
  public static void main(String[] args) {
    
    int[] createdeck = new int[52]; //create new 52 slot array
    
    for (int i = 0; i < 52; i++){ //assign 0-51 in the array
      createdeck[i] = i;
    }
    
    int[] deckShuffle = shuffle(createdeck); //call shuffle method
    
    //create array for each hand
    int[] P1hand = new int[5]; 
    int[] P2hand = new int[5];
    
    //call method for giving cards to each player
    P1hand = dealP1(deckShuffle); 
    P2hand = dealP2(deckShuffle);
    
    System.out.println("Player 1's hand:"); //print out player 1's hand
    for (int i = 0; i < 5; i++) {
      String suit = "";
      if (P1hand[i] / 13 == 0) {
        suit = "diamonds";
      }
      else if (P1hand[i] / 13 == 1) {
        suit = "clubs";
      }
      else if (P1hand[i] / 13 == 2) {
        suit = "hearts";
      }
      else if (P1hand[i] / 13 == 3) {
        suit = "spades";
      }
      
      if (P1hand[i] % 13 == 0) { //if index = 0, call it ace
        System.out.println("Ace of " + suit);
      }
      else if (P1hand[i] % 13 == 10) { //if index = 10, call it jack
        System.out.println("Jack of " + suit);
      }
      else if (P1hand[i] % 13 == 11) { //if index = 11, call it queen
        System.out.println("Queen of " + suit);
      }
      else if (P1hand[i] % 13 == 12) { //if index = 12, call it king
        System.out.println("King of " + suit);
      }
      else {
        System.out.println(((P1hand[i] % 13) + 1) + " of " + suit);
      }
      
    }
    
    System.out.println();
    
    System.out.println("Player 2's hand:"); //print out player 2's hand
    for (int i = 0; i < 5; i++) {
      String suit = "";
      if (P2hand[i] / 13 == 0) { 
        suit = "diamonds";
      }
      else if (P2hand[i] / 13 == 1) {
        suit = "clubs";
      }
      else if (P2hand[i] / 13 == 2) {
        suit = "hearts";
      }
      else if (P2hand[i] / 13 == 3) {
        suit = "spades";
      }
      
      if (P2hand[i] % 13 == 0) { //if index = 0, call it ace
        System.out.println("Ace of " + suit);
      }
      else if (P2hand[i] % 13 == 10) { //if index = 10, call it jack
        System.out.println("Jack of " + suit);
      }
      else if (P2hand[i] % 13 == 11) { //if index = 11, call it queen
        System.out.println("Queen of " + suit);
      }
      else if (P2hand[i] % 13 == 12) { //if index = 12, call it king
        System.out.println("King of " + suit);
      }
      else {
        System.out.println((P2hand[i] % 13 + 1) + " of " + suit);
      }
    }
    
    winner(P1hand, P2hand); //call winner method to decide winner
    
    
    
    
    
  }
  
  public static int[] shuffle(int[] deck){ //method for shuffling the deck randomly
    int index, temp;
    Random rand = new Random();
    for (int i = deck.length - 1; i > 0; i--) { //use a temp int to assign to spot while the values switch
        index = rand.nextInt(i + 1);
        temp = deck[index];
        deck[index] = deck[i];
        deck[i] = temp;
    }
    return deck;
  }
  
  public static int[] dealP1(int[] deck) { //deal hand to P1 on even numbers
    int[] hand1 = new int[5];
    
    hand1[0] = deck[0];
    hand1[1] = deck[2];
    hand1[2] = deck[4];
    hand1[3] = deck[6];
    hand1[4] = deck[8];
   
    return hand1;
  }
  
  public static int[] dealP2(int[] deck) { //deal hand to P2 on odd numbers
    int[] hand2 = new int[5];
    
    hand2[0] = deck[1];
    hand2[1] = deck[3];
    hand2[2] = deck[5];
    hand2[3] = deck[7];
    hand2[4] = deck[9];
    
    return hand2;
  }
  
  public static boolean pair(int[] hand) { //method to decide if a pair exists
    //get card values
    int card0val = hand[0] % 13;
    int card1val = hand[1] % 13;
    int card2val = hand[2] % 13;
    int card3val = hand[3] % 13;
    int card4val = hand[4] % 13;
    
    //do any match?
    if (card0val == card1val || card0val == card2val || card0val == card3val || card0val == card4val || card1val == card2val || card1val == card3val || card1val == card4val || card2val == card3val || card2val == card4val || card3val == card4val){
      return true; //return true if there is a match
    }
    else{return false;}
  }
  
  public static boolean threeOfAKind(int[] hand){ //method to decide if there is a 3 of a kind
    //get card values
    int card0val = hand[0] % 13;
    int card1val = hand[1] % 13;
    int card2val = hand[2] % 13;
    int card3val = hand[3] % 13;
    int card4val = hand[4] % 13;
    
    //do any match 2 others?
    boolean A = card0val == card1val && card1val == card2val; //0,1,2
    boolean B = card0val == card1val && card1val == card3val; //0,1,3
    boolean C = card0val == card1val && card1val == card4val; //0,1,4
    boolean D = card0val == card2val && card2val == card3val; //0,2,3
    boolean E = card0val == card2val && card2val == card4val; //0,2,4
    boolean F = card0val == card3val && card3val == card4val; //0,3,4
    
    boolean G = card1val == card2val && card2val == card3val; //1,2,3
    boolean H = card1val == card2val && card2val == card4val; //1,2,4
    boolean I = card1val == card3val && card3val == card4val; //1,3,4
    
    boolean J = card2val == card3val && card3val == card4val; //2,3,4
    
    boolean isThreeOfAKind = A || B || C || D || E || F || G || H || I || J;
    return isThreeOfAKind; //return true if there is a 3 match
    
    
  
  }
  
  public static boolean fullHouse(int[] hand) { //method to see if there is a full house
    //get card values
    int card0val = hand[0] % 13;
    int card1val = hand[1] % 13;
    int card2val = hand[2] % 13;
    int card3val = hand[3] % 13;
    int card4val = hand[4] % 13;
    
    //is there a 3 of a kind match along with a pair match?
    boolean A = card0val == card1val && card1val == card2val && card3val == card4val; //0,1,2
    boolean B = card0val == card1val && card1val == card3val && card2val == card4val; //0,1,3
    boolean C = card0val == card1val && card1val == card4val && card2val == card3val; //0,1,4
    boolean D = card0val == card2val && card2val == card3val && card1val == card4val; //0,2,3
    boolean E = card0val == card2val && card2val == card4val && card1val == card3val; //0,2,4
    boolean F = card0val == card3val && card3val == card4val && card1val == card2val; //0,3,4
    
    boolean G = card1val == card2val && card2val == card3val && card0val == card4val; //1,2,3
    boolean H = card1val == card2val && card2val == card4val && card0val == card3val; //1,2,4
    boolean I = card1val == card3val && card3val == card4val && card0val == card2val; //1,3,4
    
    boolean J = card2val == card3val && card3val == card4val && card0val == card1val; //2,3,4
    
    boolean isFullHouse = A || B || C || D || E || F || G || H || I || J;
    return isFullHouse; //return true if there is a full house present
  }
  
  public static boolean flush(int[] hand){ //method to see if there is a suit flush
    //get card suits
    int card0suit = hand[0] / 13;
    int card1suit = hand[1] / 13;
    int card2suit = hand[2] / 13;
    int card3suit = hand[3] / 13;
    int card4suit = hand[4] / 13;
    
    if (card0suit == card1suit && card1suit == card2suit && card2suit == card3suit && card3suit == card4suit){
      return true; //return true if all suits match
    }
    else{return false;}
  }
  
  
  public static int HighCardPair(int[] hand) { //method to determine high card in pairingn situation
    //get card values
    int card0val = hand[0] % 13;
    int card1val = hand[1] % 13;
    int card2val = hand[2] % 13;
    int card3val = hand[3] % 13;
    int card4val = hand[4] % 13;
    
    //return the correct card value of the pairing
    if (card0val == card1val || card0val == card2val || card0val == card3val || card0val == card4val) {
      return card0val;
    }
    
    else if (card1val == card2val || card1val == card3val || card1val == card4val) {
      return card1val;
    }
  
    else if (card2val == card3val || card2val == card4val) {
      return card2val;
    }
   
    else if (card3val == card4val) {
      return card3val;
    }
    else {
      return 0;
    }
  }
  
  public static int HighCard3Kind(int[] hand){ //method to determine value of 3 of a kind
    //get card values
    int card0val = hand[0] % 13;
    int card1val = hand[1] % 13;
    int card2val = hand[2] % 13;
    int card3val = hand[3] % 13;
    int card4val = hand[4] % 13;
    
    //return the correct value of the 3 of a kind
    if (card0val == card1val && card1val == card2val || card0val == card1val && card1val == card3val || card0val == card1val && card1val == card4val || card0val == card2val && card2val == card3val || card0val == card2val && card2val == card4val || card0val == card3val && card3val == card4val) {
      return card0val;
    }
    else if (card1val == card2val && card2val == card3val || card1val == card2val && card2val == card4val || card1val == card3val && card3val == card4val) {
      return card1val;
    }
    else {
      return card2val;
    }
  }
  
  public static int HighCardFlush(int[] hand) { //method to determine high card in all of hand
    //get card values
    int card0val = hand[0] % 13;
    int card1val = hand[1] % 13;
    int card2val = hand[2] % 13;
    int card3val = hand[3] % 13;
    int card4val = hand[4] % 13;
    
    //if value = 0, it is an Ace, which has high card priority
    if (card0val == 0) {
      return card0val;
    }
    else if (card1val == 0) {
      return card1val;
    }
    else if (card2val == 0) {
      return card2val;
    }
    else if (card3val == 0) {
      return card3val;
    }
    else if (card4val == 0) {
      return card4val;
    }
    //return high card if no aces are present
    else if (card0val > card1val && card0val > card2val && card0val > card3val && card0val > card4val){
      return card0val;
    }
    else if (card1val > card0val && card1val > card2val && card1val > card3val && card1val > card4val){
      return card1val;
    }
    else if (card2val > card0val && card2val > card1val && card2val > card3val && card1val > card4val){
      return card2val;
    }
    else if (card3val > card0val && card3val > card1val && card3val > card2val && card3val > card4val){
      return card3val;
    }
    else if (card4val > card0val && card4val > card1val && card4val > card2val && card4val > card3val){
      return card4val;
    }
    else {return 0;}
  }
  
  
  
  
  public static void winner(int[] hand1, int[] hand2) { //method to determine winner
    System.out.println();
    
    //get card values
    int P1card0val = hand1[0] % 13;
    int P1card1val = hand1[1] % 13;
    int P1card2val = hand1[2] % 13;
    int P1card3val = hand1[3] % 13;
    int P1card4val = hand1[4] % 13;
    
    int P2card0val = hand2[0] % 13;
    int P2card1val = hand2[1] % 13;
    int P2card2val = hand2[2] % 13;
    int P2card3val = hand2[3] % 13;
    int P2card4val = hand2[4] % 13;
    
    //call flush method to see if flush is present
    boolean P1HaveFlush = flush(hand1);
    boolean P2HaveFlush = flush(hand2);
    
    //if there is a flush present, see who has one and if they do, they win
    if (P1HaveFlush == true && P2HaveFlush == false) {
      System.out.println("P1 Wins with a flush!");
      return;
    }
    
    else if (P1HaveFlush == false && P2HaveFlush == true) {
      System.out.println("P2 Wins with a flush!");
      return;
    }
    //if both have a flush, find high card
    else if (P1HaveFlush == true && P2HaveFlush == true) {
      //call high card method
      int P1high = HighCardFlush(hand1);
      int P2high = HighCardFlush(hand2);
      
      //compare high cards
      if (P1high == 0) {
        System.out.println("P1 wins with the high card");
       }
      else if (P2high == 0) {
        System.out.println("P2 wins with the high card");
       }
      else if (P1high == 0 || P1high > P2high) {
        System.out.println("Both have a flush. P1 wins with the higher card");
        return;
      }
      else {
        System.out.println("Both have a flush. P2 wins with the higher card");
        return;
      }
    }
    
    //call full house method to determine if full house is present
    boolean P1HaveFH = fullHouse(hand1);
    boolean P2HaveFH = fullHouse(hand2);
    
    //if there is a full house present, see who has one and if they do, they win
    if (P1HaveFH == true && P2HaveFH == false) {
      System.out.println("P1 Wins with a Full House!");
      return;
    }
    
    else if (P1HaveFH == false && P2HaveFH == true) {
      System.out.println("P2 Wins with a Full House!");
      return;
    }
    //if both have a full house, find high card
    else if (P1HaveFH == true && P2HaveFH == true) {
      //call high card method
      int P1high = HighCard3Kind(hand1);
      int P2high = HighCard3Kind(hand2);
      
      //compare high cards
      if (P1high == 0) {
        System.out.println("P1 wins with the high card");
       }
      else if (P2high == 0) {
        System.out.println("P2 wins with the high card");
       }
      else if (P1high > P2high) {
        System.out.println("Both have a full house. P1 wins with the higher 3 of a kind");
        return;
      }
      else {
        System.out.println("Both have a full house. P2 wins with the higher 3 of a kind");
        return;
      }
    }
    
    
    //call 3 of a kind method to determine if 3 of a kind is present
    boolean P1Have3Kind = threeOfAKind(hand1);
    boolean P2Have3Kind = threeOfAKind(hand2);
    
    //if there is a 3 of a kind present, see who has one and if they do, they win
    if (P1Have3Kind == true && P2Have3Kind == false) {
      System.out.println("P1 Wins with a 3 of a kind!");
      return;
    }
    
    else if (P1Have3Kind == false && P2Have3Kind == true) {
      System.out.println("P2 Wins with a 3 of a kind!");
      return;
    }
    //if both have a 3 of a kind, find high card
    else if (P1Have3Kind == true && P2Have3Kind == true) {
      //call high card method
      int P1high = HighCard3Kind(hand1);
      int P2high = HighCard3Kind(hand2);
      
      //compare high cards
      if (P1high == 0) {
        System.out.println("P1 wins with the high card");
       }
      else if (P2high == 0) {
        System.out.println("P2 wins with the high card");
       }
      else if (P1high > P2high) {
        System.out.println("Both have a 3 of a kind. P1 wins with the higher 3 of a kind");
        return;
      }
      else {
        System.out.println("Both have a 3 of a kind. P2 wins with the higher 3 of a kind");
        return;
      }
    }
    
    
    //call pair method to determine if pair is present
    boolean P1HaveAPair = pair(hand1);
    boolean P2HaveAPair = pair(hand2);
    
    //if there is a pair present, see who has one and if they do, they win
    if (P1HaveAPair == true && P2HaveAPair == false) {
      System.out.println("P1 Wins with a pair!");
      return;
    }
    
    else if (P1HaveAPair == false && P2HaveAPair == true) {
      System.out.println("P2 Wins with a pair!");
      return;
    }
    
    //if both have a pair, find high card
    else if (P1HaveAPair == true && P2HaveAPair == true) {
      //call high card method
      int P1high = HighCardPair(hand1);
      int P2high = HighCardPair(hand2);
      
      //compare high cards
      if (P1high == 0) {
        System.out.println("P1 wins with the high card");
       }
      else if (P2high == 0) {
        System.out.println("P2 wins with the high card");
       }
      if (P1high > P2high) {
        System.out.println("Both have a pair. P1 wins with the higher pair");
        return;
      }
      else {
        System.out.println("Both have a pair. P2 wins with the higher pair");
        return;
      }
    }
    
    
    //if none of those apply, determine high card for the winner
    
    //call high card method
    int P1HighCard = HighCardFlush(hand1);
    int P2HighCard = HighCardFlush(hand2);
    
    //compare high cards
    if (P1HighCard == 0) {
      System.out.println("P1 wins with the high card");
    }
    else if (P2HighCard == 0) {
      System.out.println("P2 wins with the high card");
    }
    else if (P1HighCard > P2HighCard) {
      System.out.println("P1 wins with the high card");
    }
    else {
      System.out.println("P2 wins with the high card");
    }
    
    
    
    
    

  }
}//end of class











