import java.util.Scanner;

public class Exam1F12Q12 {
  public static void main(String[] args) {
    Scanner scan = new Scanner( System.in );
    System.out.println("input 3 integers");
    int number1 = scan.nextInt();
    int number2 = scan.nextInt();
    int number3 = scan.nextInt();
    
    double avg = (number1 + number2 + number3) / 3;
    System.out.println(avg);
    
    if (avg > 0) {
      System.out.println("the average is positive");
    }
    else if (avg == 0) {
      System.out.println("the average is 0");
    }
    else {
      System.out.println("the average is negative");
    }
    }
  }
   