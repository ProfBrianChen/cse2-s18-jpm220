//////////////////////////////////
///// Jeanine Minnick   Secition 211
/// CSE2 Yahtzee  2/16/18
/// Roll dice, determine which category the roll falls under, and calculate score

import java.util.Scanner;

public class YahtzeeLong {
  public static void main(String[] args) {
    
    int userNumbers = 0, diceRoll1 = 0, diceRoll2 = 0, diceRoll3 = 0, diceRoll4 = 0, diceRoll5 = 0;
    int upperScore = 0, lowerScore = 0, totalScore = 0;
    
    boolean yahtzee = true, largeStraight = true, smallStraight = true, fullHouse = true, fourOfAKind = true, threeOfAKind = true;
    boolean sixes = true, fives = true, fours = true, threes = true, twos = true, aces = true, chance = true;
    boolean upperScoreUsed = true;
    
    int onesCounter = 0, twosCounter = 0, threesCounter = 0, foursCounter = 0, fivesCounter = 0, sixesCounter = 0;
    
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Roll 1. Do you want to generate random numbers or create your own? Enter '1' for random, enter another number to create your own:");
    int startRandomOrCYO = myScanner.nextInt();
    
    
    if (startRandomOrCYO == 1) {
      diceRoll1 = (int)(Math.random() * 6) + 1;
      diceRoll2 = (int)(Math.random() * 6) + 1;
      diceRoll3 = (int)(Math.random() * 6) + 1;
      diceRoll4 = (int)(Math.random() * 6) + 1;
      diceRoll5 = (int)(Math.random() * 6) + 1;
      System.out.println("You rolled " + diceRoll1 + " " + diceRoll2 + " " + diceRoll3 + " " + diceRoll4 + " " + diceRoll5);
    }
    
    else {
      System.out.println("Enter a 5 digit number pertaining to the 5 numbers you want to roll (1-6):");
      userNumbers = myScanner.nextInt();
      diceRoll1 = (int)userNumbers / 10000;
      if (diceRoll1 < 1 || diceRoll1 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll1 * 10000);
      diceRoll2 = (int)userNumbers / 1000;
      if (diceRoll2 < 1 || diceRoll2 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll2 * 1000);
      diceRoll3 = (int)userNumbers / 100;
      if (diceRoll3 < 1 || diceRoll3 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll3 * 100);
      diceRoll4 = (int)userNumbers / 10;
      if (diceRoll4 < 1 || diceRoll4 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll4 * 10);
      diceRoll5 = (int)userNumbers;
      if (diceRoll5 < 1 || diceRoll5 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      }
    
    
    if (diceRoll1 == 1) {
      onesCounter += 1;
    }
    if (diceRoll1 == 2) {
      twosCounter += 1;
    }
    if (diceRoll1 == 3) {
      threesCounter += 1;
    }
    if (diceRoll1 == 4) {
      foursCounter += 1;
    }
    if (diceRoll1 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll1 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll2 == 1) {
      onesCounter += 1;
    }
    if (diceRoll2 == 2) {
      twosCounter += 1;
    }
    if (diceRoll2 == 3) {
      threesCounter += 1;
    }
    if (diceRoll2 == 4) {
      foursCounter += 1;
    }
    if (diceRoll2 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll2 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll3 == 1) {
      onesCounter += 1;
    }
    if (diceRoll3 == 2) {
      twosCounter += 1;
    }
    if (diceRoll3 == 3) {
      threesCounter += 1;
    }
    if (diceRoll3 == 4) {
      foursCounter += 1;
    }
    if (diceRoll3 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll3 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll4 == 1) {
      onesCounter += 1;
    }
    if (diceRoll4 == 2) {
      twosCounter += 1;
    }
    if (diceRoll4 == 3) {
      threesCounter += 1;
    }
    if (diceRoll4 == 4) {
      foursCounter += 1;
    }
    if (diceRoll4 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll4 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll5 == 1) {
      onesCounter += 1;
    }
    if (diceRoll5 == 2) {
      twosCounter += 1;
    }
    if (diceRoll5 == 3) {
      threesCounter += 1;
    }
    if (diceRoll5 == 4) {
      foursCounter += 1;
    }
    if (diceRoll5 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll5 == 6) {
      sixesCounter += 1;
    }
  
    
    System.out.println("Ones: " + onesCounter + " Twos: " + twosCounter + " Threes: " + threesCounter + " Fours: " + foursCounter + " Fives: " + fivesCounter + " Sixes: " + sixesCounter);
    
    if (yahtzee && onesCounter == 5 || twosCounter == 5 || threesCounter == 5 || foursCounter == 5 || fivesCounter == 5 || sixesCounter == 5) {
      System.out.println("You rolled a Yahtzee!");
      lowerScore += 50;
      yahtzee = false;
    }
    
    else if (largeStraight && (onesCounter == 1 && twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1) || (twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1 && sixesCounter == 1)) {
      System.out.println("You rolled a large straight");
      lowerScore += 40;
      largeStraight = false;
    }
    
    else if (smallStraight && (onesCounter >= 1 && twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1) || (twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1) || (threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1 && sixesCounter >= 1)) {
      System.out.println("You rolled a small straight");
      lowerScore += 30;
      smallStraight = false;
    }
    
    else if (fullHouse && (onesCounter == 2 || twosCounter == 2 || threesCounter == 2 || foursCounter == 2 || fivesCounter == 2 || sixesCounter == 2) && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a full house");
      lowerScore += 25;
      fullHouse = false;
    }
    
    else if (fourOfAKind && (onesCounter == 4 || twosCounter == 4 || threesCounter == 4 || foursCounter == 4 || fivesCounter == 4 || sixesCounter == 4)) {
      System.out.println("You rolled a 4 of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      fourOfAKind = false;
    }
    
    else if (threeOfAKind && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a three of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      threeOfAKind = false;
    }
    
    else if (sixesCounter >= 1 && sixes) {
      System.out.println("You rolled sixes");
      upperScore += sixesCounter * 6;
      sixes = false;
    }
    
    else if (fivesCounter >= 1 && fives) {
      System.out.println("You rolled fives");
      upperScore += fivesCounter * 5;
      fives = false;
    }
    
    else if (foursCounter >= 1 && fours) {
      System.out.println("You rolled fours");
      upperScore += foursCounter * 4;
      fours = false;
    }
    
    else if (threesCounter >= 1 && threes) {
      System.out.println("You rolled threes");
      upperScore += threesCounter * 3;
      threes = false;
    }
    
    else if (twosCounter >= 1 && twos) {
      System.out.println("You rolled twos");
      upperScore += twosCounter * 2;
      twos = false;
    }
    
    else if (onesCounter >= 1 && aces) {
      System.out.println("You rolled Aces");
      upperScore += onesCounter;
      aces = false;
    }
    
    else if (chance) {
      System.out.println("You rolled a chance");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      chance = false;
    }
    
    else {
      if (yahtzee) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Yahtzee'.");
        yahtzee = false;
      }
      else if (largeStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Large Straight'.");
        largeStraight = false;
      }
      else if (smallStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Small Straight'.");
        smallStraight = false;
      }
      else if (fullHouse) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Full House'.");
        fullHouse = false;
      }
      else if (fourOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Four of a Kind'.");
        fourOfAKind = false;
      }
      else if (threeOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Three of a Kind'.");
        threeOfAKind = false;
      }
      else if (sixes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Sixes'.");
        sixes = false;
      }
      else if (fives) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fives'.");
        fives = false;
      }
      else if (fours) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fours'.");
        fours = false;
      }
      else if (threes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Threes'.");
        threes = false;
      }
      else if (twos) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Twos'.");
        twos = false;
      }
      else {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Aces'.");
        aces = false;
      }
    }
    
    
    
    if (upperScore > 63 && upperScoreUsed) {
      System.out.println("Your upper score is greater than 63. You get a 35 point bonus");
      upperScore += 35;
      upperScoreUsed = false;
    }
    
    totalScore = upperScore + lowerScore;
    System.out.println("Upper score: " + upperScore + " Lower score: " + lowerScore + " Total Score: " + totalScore);
    
    onesCounter = 0;
    twosCounter = 0;
    threesCounter = 0;
    foursCounter = 0;
    fivesCounter = 0;
    sixesCounter = 0;
    
    
    
    
    
    
    
    System.out.println(yahtzee);
    
    
    
    
    
    
    
    
    
    
    
    
    System.out.println("Roll 2. Do you want to generate random numbers or create your own? Enter '1' for random, enter another number to create your own:");
    int startRandomOrCYO2 = myScanner.nextInt();
    
    
    if (startRandomOrCYO2 == 1) {
      diceRoll1 = (int)(Math.random() * 6) + 1;
      diceRoll2 = (int)(Math.random() * 6) + 1;
      diceRoll3 = (int)(Math.random() * 6) + 1;
      diceRoll4 = (int)(Math.random() * 6) + 1;
      diceRoll5 = (int)(Math.random() * 6) + 1;
      System.out.println("You rolled " + diceRoll1 + " " + diceRoll2 + " " + diceRoll3 + " " + diceRoll4 + " " + diceRoll5);
    }
    
    else {
      System.out.println("Enter a 5 digit number pertaining to the 5 numbers you want to roll (1-6):");
      userNumbers = myScanner.nextInt();
      diceRoll1 = (int)userNumbers / 10000;
      if (diceRoll1 < 1 || diceRoll1 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll1 * 10000);
      diceRoll2 = (int)userNumbers / 1000;
      if (diceRoll2 < 1 || diceRoll2 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll2 * 1000);
      diceRoll3 = (int)userNumbers / 100;
      if (diceRoll3 < 1 || diceRoll3 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll3 * 100);
      diceRoll4 = (int)userNumbers / 10;
      if (diceRoll4 < 1 || diceRoll4 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll4 * 10);
      diceRoll5 = (int)userNumbers;
      if (diceRoll5 < 1 || diceRoll5 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      }
    
    
    if (diceRoll1 == 1) {
      onesCounter += 1;
    }
    if (diceRoll1 == 2) {
      twosCounter += 1;
    }
    if (diceRoll1 == 3) {
      threesCounter += 1;
    }
    if (diceRoll1 == 4) {
      foursCounter += 1;
    }
    if (diceRoll1 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll1 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll2 == 1) {
      onesCounter += 1;
    }
    if (diceRoll2 == 2) {
      twosCounter += 1;
    }
    if (diceRoll2 == 3) {
      threesCounter += 1;
    }
    if (diceRoll2 == 4) {
      foursCounter += 1;
    }
    if (diceRoll2 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll2 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll3 == 1) {
      onesCounter += 1;
    }
    if (diceRoll3 == 2) {
      twosCounter += 1;
    }
    if (diceRoll3 == 3) {
      threesCounter += 1;
    }
    if (diceRoll3 == 4) {
      foursCounter += 1;
    }
    if (diceRoll3 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll3 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll4 == 1) {
      onesCounter += 1;
    }
    if (diceRoll4 == 2) {
      twosCounter += 1;
    }
    if (diceRoll4 == 3) {
      threesCounter += 1;
    }
    if (diceRoll4 == 4) {
      foursCounter += 1;
    }
    if (diceRoll4 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll4 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll5 == 1) {
      onesCounter += 1;
    }
    if (diceRoll5 == 2) {
      twosCounter += 1;
    }
    if (diceRoll5 == 3) {
      threesCounter += 1;
    }
    if (diceRoll5 == 4) {
      foursCounter += 1;
    }
    if (diceRoll5 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll5 == 6) {
      sixesCounter += 1;
    }
  
    
    System.out.println("Ones: " + onesCounter + " Twos: " + twosCounter + " Threes: " + threesCounter + " Fours: " + foursCounter + " Fives: " + fivesCounter + " Sixes: " + sixesCounter);
    
    if (yahtzee && (onesCounter == 5 || twosCounter == 5 || threesCounter == 5 || foursCounter == 5 || fivesCounter == 5 || sixesCounter == 5)) {
      System.out.println("You rolled a Yahtzee!");
      lowerScore += 50;
      yahtzee = false;
    }
    
    else if (largeStraight && (onesCounter == 1 && twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1) || (twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1 && sixesCounter == 1)) {
      System.out.println("You rolled a large straight");
      lowerScore += 40;
      largeStraight = false;
    }
    
    else if (smallStraight && (onesCounter >= 1 && twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1) || (twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1) || (threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1 && sixesCounter >= 1)) {
      System.out.println("You rolled a small straight");
      lowerScore += 30;
      smallStraight = false;
    }
    
    else if (fullHouse && (onesCounter == 2 || twosCounter == 2 || threesCounter == 2 || foursCounter == 2 || fivesCounter == 2 || sixesCounter == 2) && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a full house");
      lowerScore += 25;
      fullHouse = false;
    }
    
    else if (fourOfAKind && (onesCounter >= 4 || twosCounter >= 4 || threesCounter >= 4 || foursCounter >= 4 || fivesCounter >= 4 || sixesCounter >= 4)) {
      System.out.println("You rolled a 4 of a kind");
      lowerScore += diceRoll5 + diceRoll4 + diceRoll3 + diceRoll2 + diceRoll1;
      fourOfAKind = false;
    }
    
    else if (threeOfAKind && (onesCounter >= 3 || twosCounter >= 3 || threesCounter >= 3 || foursCounter >= 3 || fivesCounter >= 3 || sixesCounter >= 3)) {
      System.out.println("You rolled a three of a kind");
      lowerScore += diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      threeOfAKind = false;
    }
    
    else if (sixesCounter >= 1 && sixes) {
      System.out.println("You rolled sixes");
      upperScore += sixesCounter * 6;
      sixes = false;
    }
    
    else if (fivesCounter >= 1 && fives) {
      System.out.println("You rolled fives");
      upperScore += fivesCounter * 5;
      fives = false;
    }
    
    else if (foursCounter >= 1 && fours) {
      System.out.println("You rolled fours");
      upperScore += foursCounter * 4;
      fours = false;
    }
    
    else if (threesCounter >= 1 && threes) {
      System.out.println("You rolled threes");
      upperScore += threesCounter * 3;
      threes = false;
    }
    
    else if (twosCounter >= 1 && twos) {
      System.out.println("You rolled twos");
      upperScore += twosCounter * 2;
      twos = false;
    }
    
    else if (onesCounter >= 1 && aces) {
      System.out.println("You rolled Aces");
      upperScore += onesCounter;
      aces = false;
    }
    
    else if (chance) {
      System.out.println("You rolled a chance");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      chance = false;
    }
    
    else {
      if (yahtzee) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Yahtzee'.");
        yahtzee = false;
      }
      else if (largeStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Large Straight'.");
        largeStraight = false;
      }
      else if (smallStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Small Straight'.");
        smallStraight = false;
      }
      else if (fullHouse) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Full House'.");
        fullHouse = false;
      }
      else if (fourOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Four of a Kind'.");
        fourOfAKind = false;
      }
      else if (threeOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Three of a Kind'.");
        threeOfAKind = false;
      }
      else if (sixes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Sixes'.");
        sixes = false;
      }
      else if (fives) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fives'.");
        fives = false;
      }
      else if (fours) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fours'.");
        fours = false;
      }
      else if (threes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Threes'.");
        threes = false;
      }
      else if (twos) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Twos'.");
        twos = false;
      }
      else {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Aces'.");
        aces = false;
      }
    }
    
    
    if (upperScore > 63 && upperScoreUsed) {
      System.out.println("Your upper score is greater than 63. You get a 35 point bonus");
      upperScore += 35;
      upperScoreUsed = false;
    }
    
    totalScore = upperScore + lowerScore;
    System.out.println("Upper score: " + upperScore + " Lower score: " + lowerScore + " Total Score: " + totalScore);
    
    onesCounter = 0;
    twosCounter = 0;
    threesCounter = 0;
    foursCounter = 0;
    fivesCounter = 0;
    sixesCounter = 0;
    
    
    
    
    
    
    System.out.println(yahtzee);
    
    
    
    
    
    
    
    
    System.out.println("Roll 3. Do you want to generate random numbers or create your own? Enter '1' for random, enter another number to create your own:");
    int startRandomOrCYO3 = myScanner.nextInt();
    
    
    if (startRandomOrCYO3 == 1) {
      diceRoll1 = (int)(Math.random() * 6) + 1;
      diceRoll2 = (int)(Math.random() * 6) + 1;
      diceRoll3 = (int)(Math.random() * 6) + 1;
      diceRoll4 = (int)(Math.random() * 6) + 1;
      diceRoll5 = (int)(Math.random() * 6) + 1;
      System.out.println("You rolled " + diceRoll1 + " " + diceRoll2 + " " + diceRoll3 + " " + diceRoll4 + " " + diceRoll5);
    }
    
    else {
      System.out.println("Enter a 5 digit number pertaining to the 5 numbers you want to roll (1-6):");
      userNumbers = myScanner.nextInt();
      diceRoll1 = (int)userNumbers / 10000;
      if (diceRoll1 < 1 || diceRoll1 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll1 * 10000);
      diceRoll2 = (int)userNumbers / 1000;
      if (diceRoll2 < 1 || diceRoll2 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll2 * 1000);
      diceRoll3 = (int)userNumbers / 100;
      if (diceRoll3 < 1 || diceRoll3 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll3 * 100);
      diceRoll4 = (int)userNumbers / 10;
      if (diceRoll4 < 1 || diceRoll4 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll4 * 10);
      diceRoll5 = (int)userNumbers;
      if (diceRoll5 < 1 || diceRoll5 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      }
    
    
    if (diceRoll1 == 1) {
      onesCounter += 1;
    }
    if (diceRoll1 == 2) {
      twosCounter += 1;
    }
    if (diceRoll1 == 3) {
      threesCounter += 1;
    }
    if (diceRoll1 == 4) {
      foursCounter += 1;
    }
    if (diceRoll1 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll1 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll2 == 1) {
      onesCounter += 1;
    }
    if (diceRoll2 == 2) {
      twosCounter += 1;
    }
    if (diceRoll2 == 3) {
      threesCounter += 1;
    }
    if (diceRoll2 == 4) {
      foursCounter += 1;
    }
    if (diceRoll2 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll2 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll3 == 1) {
      onesCounter += 1;
    }
    if (diceRoll3 == 2) {
      twosCounter += 1;
    }
    if (diceRoll3 == 3) {
      threesCounter += 1;
    }
    if (diceRoll3 == 4) {
      foursCounter += 1;
    }
    if (diceRoll3 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll3 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll4 == 1) {
      onesCounter += 1;
    }
    if (diceRoll4 == 2) {
      twosCounter += 1;
    }
    if (diceRoll4 == 3) {
      threesCounter += 1;
    }
    if (diceRoll4 == 4) {
      foursCounter += 1;
    }
    if (diceRoll4 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll4 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll5 == 1) {
      onesCounter += 1;
    }
    if (diceRoll5 == 2) {
      twosCounter += 1;
    }
    if (diceRoll5 == 3) {
      threesCounter += 1;
    }
    if (diceRoll5 == 4) {
      foursCounter += 1;
    }
    if (diceRoll5 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll5 == 6) {
      sixesCounter += 1;
    }
  
    
    System.out.println("Ones: " + onesCounter + " Twos: " + twosCounter + " Threes: " + threesCounter + " Fours: " + foursCounter + " Fives: " + fivesCounter + " Sixes: " + sixesCounter);
    
    if (yahtzee && onesCounter == 5 || twosCounter == 5 || threesCounter == 5 || foursCounter == 5 || fivesCounter == 5 || sixesCounter == 5) {
      System.out.println("You rolled a Yahtzee!");
      lowerScore += 50;
      yahtzee = false;
    }
    
    else if (largeStraight && (onesCounter == 1 && twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1) || (twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1 && sixesCounter == 1)) {
      System.out.println("You rolled a large straight");
      lowerScore += 40;
      largeStraight = false;
    }
    
    else if (smallStraight && (onesCounter >= 1 && twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1) || (twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1) || (threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1 && sixesCounter >= 1)) {
      System.out.println("You rolled a small straight");
      lowerScore += 30;
      smallStraight = false;
    }
    
    else if (fullHouse && (onesCounter == 2 || twosCounter == 2 || threesCounter == 2 || foursCounter == 2 || fivesCounter == 2 || sixesCounter == 2) && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a full house");
      lowerScore += 25;
      fullHouse = false;
    }
    
    else if (fourOfAKind && (onesCounter == 4 || twosCounter == 4 || threesCounter == 4 || foursCounter == 4 || fivesCounter == 4 || sixesCounter == 4)) {
      System.out.println("You rolled a 4 of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      fourOfAKind = false;
    }
    
    else if (threeOfAKind && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a three of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      threeOfAKind = false;
    }
    
    else if (sixesCounter >= 1 && sixes) {
      System.out.println("You rolled sixes");
      upperScore += sixesCounter * 6;
      sixes = false;
    }
    
    else if (fivesCounter >= 1 && fives) {
      System.out.println("You rolled fives");
      upperScore += fivesCounter * 5;
      fives = false;
    }
    
    else if (foursCounter >= 1 && fours) {
      System.out.println("You rolled fours");
      upperScore += foursCounter * 4;
      fours = false;
    }
    
    else if (threesCounter >= 1 && threes) {
      System.out.println("You rolled threes");
      upperScore += threesCounter * 3;
      threes = false;
    }
    
    else if (twosCounter >= 1 && twos) {
      System.out.println("You rolled twos");
      upperScore += twosCounter * 2;
      twos = false;
    }
    
    else if (onesCounter >= 1 && aces) {
      System.out.println("You rolled Aces");
      upperScore += onesCounter;
      aces = false;
    }
    
    else if (chance) {
      System.out.println("You rolled a chance");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      chance = false;
    }
    
    else {
      if (yahtzee) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Yahtzee'.");
        yahtzee = false;
      }
      else if (largeStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Large Straight'.");
        largeStraight = false;
      }
      else if (smallStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Small Straight'.");
        smallStraight = false;
      }
      else if (fullHouse) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Full House'.");
        fullHouse = false;
      }
      else if (fourOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Four of a Kind'.");
        fourOfAKind = false;
      }
      else if (threeOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Three of a Kind'.");
        threeOfAKind = false;
      }
      else if (sixes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Sixes'.");
        sixes = false;
      }
      else if (fives) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fives'.");
        fives = false;
      }
      else if (fours) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fours'.");
        fours = false;
      }
      else if (threes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Threes'.");
        threes = false;
      }
      else if (twos) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Twos'.");
        twos = false;
      }
      else {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Aces'.");
        aces = false;
      }
    }
    
    
    if (upperScore > 63 && upperScoreUsed) {
      System.out.println("Your upper score is greater than 63. You get a 35 point bonus");
      upperScore += 35;
      upperScoreUsed = false;
    }
    
    totalScore = upperScore + lowerScore;
    System.out.println("Upper score: " + upperScore + " Lower score: " + lowerScore + " Total Score: " + totalScore);
    
    onesCounter = 0;
    twosCounter = 0;
    threesCounter = 0;
    foursCounter = 0;
    fivesCounter = 0;
    sixesCounter = 0;
    
    
    
    
    
    
    
    
    System.out.println(yahtzee);
    
    
    
    
    
    
    System.out.println("Roll 4. Do you want to generate random numbers or create your own? Enter '1' for random, enter another number to create your own:");
    int startRandomOrCYO4 = myScanner.nextInt();
    
    
    if (startRandomOrCYO4 == 1) {
      diceRoll1 = (int)(Math.random() * 6) + 1;
      diceRoll2 = (int)(Math.random() * 6) + 1;
      diceRoll3 = (int)(Math.random() * 6) + 1;
      diceRoll4 = (int)(Math.random() * 6) + 1;
      diceRoll5 = (int)(Math.random() * 6) + 1;
      System.out.println("You rolled " + diceRoll1 + " " + diceRoll2 + " " + diceRoll3 + " " + diceRoll4 + " " + diceRoll5);
    }
    
    else {
      System.out.println("Enter a 5 digit number pertaining to the 5 numbers you want to roll (1-6):");
      userNumbers = myScanner.nextInt();
      diceRoll1 = (int)userNumbers / 10000;
      if (diceRoll1 < 1 || diceRoll1 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll1 * 10000);
      diceRoll2 = (int)userNumbers / 1000;
      if (diceRoll2 < 1 || diceRoll2 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll2 * 1000);
      diceRoll3 = (int)userNumbers / 100;
      if (diceRoll3 < 1 || diceRoll3 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll3 * 100);
      diceRoll4 = (int)userNumbers / 10;
      if (diceRoll4 < 1 || diceRoll4 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll4 * 10);
      diceRoll5 = (int)userNumbers;
      if (diceRoll5 < 1 || diceRoll5 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      }
    
    
    if (diceRoll1 == 1) {
      onesCounter += 1;
    }
    if (diceRoll1 == 2) {
      twosCounter += 1;
    }
    if (diceRoll1 == 3) {
      threesCounter += 1;
    }
    if (diceRoll1 == 4) {
      foursCounter += 1;
    }
    if (diceRoll1 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll1 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll2 == 1) {
      onesCounter += 1;
    }
    if (diceRoll2 == 2) {
      twosCounter += 1;
    }
    if (diceRoll2 == 3) {
      threesCounter += 1;
    }
    if (diceRoll2 == 4) {
      foursCounter += 1;
    }
    if (diceRoll2 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll2 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll3 == 1) {
      onesCounter += 1;
    }
    if (diceRoll3 == 2) {
      twosCounter += 1;
    }
    if (diceRoll3 == 3) {
      threesCounter += 1;
    }
    if (diceRoll3 == 4) {
      foursCounter += 1;
    }
    if (diceRoll3 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll3 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll4 == 1) {
      onesCounter += 1;
    }
    if (diceRoll4 == 2) {
      twosCounter += 1;
    }
    if (diceRoll4 == 3) {
      threesCounter += 1;
    }
    if (diceRoll4 == 4) {
      foursCounter += 1;
    }
    if (diceRoll4 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll4 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll5 == 1) {
      onesCounter += 1;
    }
    if (diceRoll5 == 2) {
      twosCounter += 1;
    }
    if (diceRoll5 == 3) {
      threesCounter += 1;
    }
    if (diceRoll5 == 4) {
      foursCounter += 1;
    }
    if (diceRoll5 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll5 == 6) {
      sixesCounter += 1;
    }
  
    
    System.out.println("Ones: " + onesCounter + " Twos: " + twosCounter + " Threes: " + threesCounter + " Fours: " + foursCounter + " Fives: " + fivesCounter + " Sixes: " + sixesCounter);
    
    if (yahtzee && onesCounter == 5 || twosCounter == 5 || threesCounter == 5 || foursCounter == 5 || fivesCounter == 5 || sixesCounter == 5) {
      System.out.println("You rolled a Yahtzee!");
      lowerScore += 50;
      yahtzee = false;
    }
    
    else if (largeStraight && (onesCounter == 1 && twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1) || (twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1 && sixesCounter == 1)) {
      System.out.println("You rolled a large straight");
      lowerScore += 40;
      largeStraight = false;
    }
    
    else if (smallStraight && (onesCounter >= 1 && twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1) || (twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1) || (threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1 && sixesCounter >= 1)) {
      System.out.println("You rolled a small straight");
      lowerScore += 30;
      smallStraight = false;
    }
    
    else if (fullHouse && (onesCounter == 2 || twosCounter == 2 || threesCounter == 2 || foursCounter == 2 || fivesCounter == 2 || sixesCounter == 2) && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a full house");
      lowerScore += 25;
      fullHouse = false;
    }
    
    else if (fourOfAKind && (onesCounter == 4 || twosCounter == 4 || threesCounter == 4 || foursCounter == 4 || fivesCounter == 4 || sixesCounter == 4)) {
      System.out.println("You rolled a 4 of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      fourOfAKind = false;
    }
    
    else if (threeOfAKind && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a three of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      threeOfAKind = false;
    }
    
    else if (sixesCounter >= 1 && sixes) {
      System.out.println("You rolled sixes");
      upperScore += sixesCounter * 6;
      sixes = false;
    }
    
    else if (fivesCounter >= 1 && fives) {
      System.out.println("You rolled fives");
      upperScore += fivesCounter * 5;
      fives = false;
    }
    
    else if (foursCounter >= 1 && fours) {
      System.out.println("You rolled fours");
      upperScore += foursCounter * 4;
      fours = false;
    }
    
    else if (threesCounter >= 1 && threes) {
      System.out.println("You rolled threes");
      upperScore += threesCounter * 3;
      threes = false;
    }
    
    else if (twosCounter >= 1 && twos) {
      System.out.println("You rolled twos");
      upperScore += twosCounter * 2;
      twos = false;
    }
    
    else if (onesCounter >= 1 && aces) {
      System.out.println("You rolled Aces");
      upperScore += onesCounter;
      aces = false;
    }
    
    else if (chance) {
      System.out.println("You rolled a chance");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      chance = false;
    }
    
    else {
      if (yahtzee) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Yahtzee'.");
        yahtzee = false;
      }
      else if (largeStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Large Straight'.");
        largeStraight = false;
      }
      else if (smallStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Small Straight'.");
        smallStraight = false;
      }
      else if (fullHouse) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Full House'.");
        fullHouse = false;
      }
      else if (fourOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Four of a Kind'.");
        fourOfAKind = false;
      }
      else if (threeOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Three of a Kind'.");
        threeOfAKind = false;
      }
      else if (sixes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Sixes'.");
        sixes = false;
      }
      else if (fives) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fives'.");
        fives = false;
      }
      else if (fours) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fours'.");
        fours = false;
      }
      else if (threes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Threes'.");
        threes = false;
      }
      else if (twos) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Twos'.");
        twos = false;
      }
      else {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Aces'.");
        aces = false;
      }
    }
    
    
    
    if (upperScore > 63 && upperScoreUsed) {
      System.out.println("Your upper score is greater than 63. You get a 35 point bonus");
      upperScore += 35;
      upperScoreUsed = false;
    }
    
    totalScore = upperScore + lowerScore;
    System.out.println("Upper score: " + upperScore + " Lower score: " + lowerScore + " Total Score: " + totalScore);
    
    onesCounter = 0;
    twosCounter = 0;
    threesCounter = 0;
    foursCounter = 0;
    fivesCounter = 0;
    sixesCounter = 0;
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    System.out.println("Roll 5. Do you want to generate random numbers or create your own? Enter '1' for random, enter another number to create your own:");
    int startRandomOrCYO5 = myScanner.nextInt();
    
    
    if (startRandomOrCYO5 == 1) {
      diceRoll1 = (int)(Math.random() * 6) + 1;
      diceRoll2 = (int)(Math.random() * 6) + 1;
      diceRoll3 = (int)(Math.random() * 6) + 1;
      diceRoll4 = (int)(Math.random() * 6) + 1;
      diceRoll5 = (int)(Math.random() * 6) + 1;
      System.out.println("You rolled " + diceRoll1 + " " + diceRoll2 + " " + diceRoll3 + " " + diceRoll4 + " " + diceRoll5);
    }
    
    else {
      System.out.println("Enter a 5 digit number pertaining to the 5 numbers you want to roll (1-6):");
      userNumbers = myScanner.nextInt();
      diceRoll1 = (int)userNumbers / 10000;
      if (diceRoll1 < 1 || diceRoll1 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll1 * 10000);
      diceRoll2 = (int)userNumbers / 1000;
      if (diceRoll2 < 1 || diceRoll2 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll2 * 1000);
      diceRoll3 = (int)userNumbers / 100;
      if (diceRoll3 < 1 || diceRoll3 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll3 * 100);
      diceRoll4 = (int)userNumbers / 10;
      if (diceRoll4 < 1 || diceRoll4 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll4 * 10);
      diceRoll5 = (int)userNumbers;
      if (diceRoll5 < 1 || diceRoll5 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      }
    
    
    if (diceRoll1 == 1) {
      onesCounter += 1;
    }
    if (diceRoll1 == 2) {
      twosCounter += 1;
    }
    if (diceRoll1 == 3) {
      threesCounter += 1;
    }
    if (diceRoll1 == 4) {
      foursCounter += 1;
    }
    if (diceRoll1 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll1 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll2 == 1) {
      onesCounter += 1;
    }
    if (diceRoll2 == 2) {
      twosCounter += 1;
    }
    if (diceRoll2 == 3) {
      threesCounter += 1;
    }
    if (diceRoll2 == 4) {
      foursCounter += 1;
    }
    if (diceRoll2 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll2 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll3 == 1) {
      onesCounter += 1;
    }
    if (diceRoll3 == 2) {
      twosCounter += 1;
    }
    if (diceRoll3 == 3) {
      threesCounter += 1;
    }
    if (diceRoll3 == 4) {
      foursCounter += 1;
    }
    if (diceRoll3 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll3 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll4 == 1) {
      onesCounter += 1;
    }
    if (diceRoll4 == 2) {
      twosCounter += 1;
    }
    if (diceRoll4 == 3) {
      threesCounter += 1;
    }
    if (diceRoll4 == 4) {
      foursCounter += 1;
    }
    if (diceRoll4 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll4 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll5 == 1) {
      onesCounter += 1;
    }
    if (diceRoll5 == 2) {
      twosCounter += 1;
    }
    if (diceRoll5 == 3) {
      threesCounter += 1;
    }
    if (diceRoll5 == 4) {
      foursCounter += 1;
    }
    if (diceRoll5 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll5 == 6) {
      sixesCounter += 1;
    }
  
    
    System.out.println("Ones: " + onesCounter + " Twos: " + twosCounter + " Threes: " + threesCounter + " Fours: " + foursCounter + " Fives: " + fivesCounter + " Sixes: " + sixesCounter);
    
    if (yahtzee && onesCounter == 5 || twosCounter == 5 || threesCounter == 5 || foursCounter == 5 || fivesCounter == 5 || sixesCounter == 5) {
      System.out.println("You rolled a Yahtzee!");
      lowerScore += 50;
      yahtzee = false;
    }
    
    else if (largeStraight && (onesCounter == 1 && twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1) || (twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1 && sixesCounter == 1)) {
      System.out.println("You rolled a large straight");
      lowerScore += 40;
      largeStraight = false;
    }
    
    else if (smallStraight && (onesCounter >= 1 && twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1) || (twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1) || (threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1 && sixesCounter >= 1)) {
      System.out.println("You rolled a small straight");
      lowerScore += 30;
      smallStraight = false;
    }
    
    else if (fullHouse && (onesCounter == 2 || twosCounter == 2 || threesCounter == 2 || foursCounter == 2 || fivesCounter == 2 || sixesCounter == 2) && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a full house");
      lowerScore += 25;
      fullHouse = false;
    }
    
    else if (fourOfAKind && (onesCounter == 4 || twosCounter == 4 || threesCounter == 4 || foursCounter == 4 || fivesCounter == 4 || sixesCounter == 4)) {
      System.out.println("You rolled a 4 of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      fourOfAKind = false;
    }
    
    else if (threeOfAKind && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a three of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      threeOfAKind = false;
    }
    
    else if (sixesCounter >= 1 && sixes) {
      System.out.println("You rolled sixes");
      upperScore += sixesCounter * 6;
      sixes = false;
    }
    
    else if (fivesCounter >= 1 && fives) {
      System.out.println("You rolled fives");
      upperScore += fivesCounter * 5;
      fives = false;
    }
    
    else if (foursCounter >= 1 && fours) {
      System.out.println("You rolled fours");
      upperScore += foursCounter * 4;
      fours = false;
    }
    
    else if (threesCounter >= 1 && threes) {
      System.out.println("You rolled threes");
      upperScore += threesCounter * 3;
      threes = false;
    }
    
    else if (twosCounter >= 1 && twos) {
      System.out.println("You rolled twos");
      upperScore += twosCounter * 2;
      twos = false;
    }
    
    else if (onesCounter >= 1 && aces) {
      System.out.println("You rolled Aces");
      upperScore += onesCounter;
      aces = false;
    }
    
    else if (chance) {
      System.out.println("You rolled a chance");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      chance = false;
    }
    
    else {
      if (yahtzee) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Yahtzee'.");
        yahtzee = false;
      }
      else if (largeStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Large Straight'.");
        largeStraight = false;
      }
      else if (smallStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Small Straight'.");
        smallStraight = false;
      }
      else if (fullHouse) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Full House'.");
        fullHouse = false;
      }
      else if (fourOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Four of a Kind'.");
        fourOfAKind = false;
      }
      else if (threeOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Three of a Kind'.");
        threeOfAKind = false;
      }
      else if (sixes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Sixes'.");
        sixes = false;
      }
      else if (fives) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fives'.");
        fives = false;
      }
      else if (fours) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fours'.");
        fours = false;
      }
      else if (threes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Threes'.");
        threes = false;
      }
      else if (twos) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Twos'.");
        twos = false;
      }
      else {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Aces'.");
        aces = false;
      }
    }
    
    
    
    if (upperScore > 63 && upperScoreUsed) {
      System.out.println("Your upper score is greater than 63. You get a 35 point bonus");
      upperScore += 35;
      upperScoreUsed = false;
    }
    
    totalScore = upperScore + lowerScore;
    System.out.println("Upper score: " + upperScore + " Lower score: " + lowerScore + " Total Score: " + totalScore);
    
    onesCounter = 0;
    twosCounter = 0;
    threesCounter = 0;
    foursCounter = 0;
    fivesCounter = 0;
    sixesCounter = 0;
    
    
    
    
    
    
    
    
    
    
    
    
    
    System.out.println("Roll 6. Do you want to generate random numbers or create your own? Enter '1' for random, enter another number to create your own:");
    int startRandomOrCYO6 = myScanner.nextInt();
    
    
    if (startRandomOrCYO6 == 1) {
      diceRoll1 = (int)(Math.random() * 6) + 1;
      diceRoll2 = (int)(Math.random() * 6) + 1;
      diceRoll3 = (int)(Math.random() * 6) + 1;
      diceRoll4 = (int)(Math.random() * 6) + 1;
      diceRoll5 = (int)(Math.random() * 6) + 1;
      System.out.println("You rolled " + diceRoll1 + " " + diceRoll2 + " " + diceRoll3 + " " + diceRoll4 + " " + diceRoll5);
    }
    
    else {
      System.out.println("Enter a 5 digit number pertaining to the 5 numbers you want to roll (1-6):");
      userNumbers = myScanner.nextInt();
      diceRoll1 = (int)userNumbers / 10000;
      if (diceRoll1 < 1 || diceRoll1 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll1 * 10000);
      diceRoll2 = (int)userNumbers / 1000;
      if (diceRoll2 < 1 || diceRoll2 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll2 * 1000);
      diceRoll3 = (int)userNumbers / 100;
      if (diceRoll3 < 1 || diceRoll3 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll3 * 100);
      diceRoll4 = (int)userNumbers / 10;
      if (diceRoll4 < 1 || diceRoll4 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll4 * 10);
      diceRoll5 = (int)userNumbers;
      if (diceRoll5 < 1 || diceRoll5 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      }
    
    
    if (diceRoll1 == 1) {
      onesCounter += 1;
    }
    if (diceRoll1 == 2) {
      twosCounter += 1;
    }
    if (diceRoll1 == 3) {
      threesCounter += 1;
    }
    if (diceRoll1 == 4) {
      foursCounter += 1;
    }
    if (diceRoll1 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll1 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll2 == 1) {
      onesCounter += 1;
    }
    if (diceRoll2 == 2) {
      twosCounter += 1;
    }
    if (diceRoll2 == 3) {
      threesCounter += 1;
    }
    if (diceRoll2 == 4) {
      foursCounter += 1;
    }
    if (diceRoll2 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll2 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll3 == 1) {
      onesCounter += 1;
    }
    if (diceRoll3 == 2) {
      twosCounter += 1;
    }
    if (diceRoll3 == 3) {
      threesCounter += 1;
    }
    if (diceRoll3 == 4) {
      foursCounter += 1;
    }
    if (diceRoll3 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll3 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll4 == 1) {
      onesCounter += 1;
    }
    if (diceRoll4 == 2) {
      twosCounter += 1;
    }
    if (diceRoll4 == 3) {
      threesCounter += 1;
    }
    if (diceRoll4 == 4) {
      foursCounter += 1;
    }
    if (diceRoll4 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll4 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll5 == 1) {
      onesCounter += 1;
    }
    if (diceRoll5 == 2) {
      twosCounter += 1;
    }
    if (diceRoll5 == 3) {
      threesCounter += 1;
    }
    if (diceRoll5 == 4) {
      foursCounter += 1;
    }
    if (diceRoll5 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll5 == 6) {
      sixesCounter += 1;
    }
  
    
    System.out.println("Ones: " + onesCounter + " Twos: " + twosCounter + " Threes: " + threesCounter + " Fours: " + foursCounter + " Fives: " + fivesCounter + " Sixes: " + sixesCounter);
    
    if (yahtzee && onesCounter == 5 || twosCounter == 5 || threesCounter == 5 || foursCounter == 5 || fivesCounter == 5 || sixesCounter == 5) {
      System.out.println("You rolled a Yahtzee!");
      lowerScore += 50;
      yahtzee = false;
    }
    
    else if (largeStraight && (onesCounter == 1 && twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1) || (twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1 && sixesCounter == 1)) {
      System.out.println("You rolled a large straight");
      lowerScore += 40;
      largeStraight = false;
    }
    
    else if (smallStraight && (onesCounter >= 1 && twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1) || (twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1) || (threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1 && sixesCounter >= 1)) {
      System.out.println("You rolled a small straight");
      lowerScore += 30;
      smallStraight = false;
    }
    
    else if (fullHouse && (onesCounter == 2 || twosCounter == 2 || threesCounter == 2 || foursCounter == 2 || fivesCounter == 2 || sixesCounter == 2) && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a full house");
      lowerScore += 25;
      fullHouse = false;
    }
    
    else if (fourOfAKind && (onesCounter == 4 || twosCounter == 4 || threesCounter == 4 || foursCounter == 4 || fivesCounter == 4 || sixesCounter == 4)) {
      System.out.println("You rolled a 4 of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      fourOfAKind = false;
    }
    
    else if (threeOfAKind && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a three of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      threeOfAKind = false;
    }
    
    else if (sixesCounter >= 1 && sixes) {
      System.out.println("You rolled sixes");
      upperScore += sixesCounter * 6;
      sixes = false;
    }
    
    else if (fivesCounter >= 1 && fives) {
      System.out.println("You rolled fives");
      upperScore += fivesCounter * 5;
      fives = false;
    }
    
    else if (foursCounter >= 1 && fours) {
      System.out.println("You rolled fours");
      upperScore += foursCounter * 4;
      fours = false;
    }
    
    else if (threesCounter >= 1 && threes) {
      System.out.println("You rolled threes");
      upperScore += threesCounter * 3;
      threes = false;
    }
    
    else if (twosCounter >= 1 && twos) {
      System.out.println("You rolled twos");
      upperScore += twosCounter * 2;
      twos = false;
    }
    
    else if (onesCounter >= 1 && aces) {
      System.out.println("You rolled Aces");
      upperScore += onesCounter;
      aces = false;
    }
    
    else if (chance) {
      System.out.println("You rolled a chance");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      chance = false;
    }
    
    else {
      if (yahtzee) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Yahtzee'.");
        yahtzee = false;
      }
      else if (largeStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Large Straight'.");
        largeStraight = false;
      }
      else if (smallStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Small Straight'.");
        smallStraight = false;
      }
      else if (fullHouse) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Full House'.");
        fullHouse = false;
      }
      else if (fourOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Four of a Kind'.");
        fourOfAKind = false;
      }
      else if (threeOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Three of a Kind'.");
        threeOfAKind = false;
      }
      else if (sixes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Sixes'.");
        sixes = false;
      }
      else if (fives) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fives'.");
        fives = false;
      }
      else if (fours) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fours'.");
        fours = false;
      }
      else if (threes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Threes'.");
        threes = false;
      }
      else if (twos) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Twos'.");
        twos = false;
      }
      else {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Aces'.");
        aces = false;
      }
    }
    
    
    
    if (upperScore > 63 && upperScoreUsed) {
      System.out.println("Your upper score is greater than 63. You get a 35 point bonus");
      upperScore += 35;
      upperScoreUsed = false;
    }
    
    totalScore = upperScore + lowerScore;
    System.out.println("Upper score: " + upperScore + " Lower score: " + lowerScore + " Total Score: " + totalScore);
    
    onesCounter = 0;
    twosCounter = 0;
    threesCounter = 0;
    foursCounter = 0;
    fivesCounter = 0;
    sixesCounter = 0;
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    System.out.println("Roll 7. Do you want to generate random numbers or create your own? Enter '1' for random, enter another number to create your own:");
    int startRandomOrCYO7 = myScanner.nextInt();
    
    
    if (startRandomOrCYO7 == 1) {
      diceRoll1 = (int)(Math.random() * 6) + 1;
      diceRoll2 = (int)(Math.random() * 6) + 1;
      diceRoll3 = (int)(Math.random() * 6) + 1;
      diceRoll4 = (int)(Math.random() * 6) + 1;
      diceRoll5 = (int)(Math.random() * 6) + 1;
      System.out.println("You rolled " + diceRoll1 + " " + diceRoll2 + " " + diceRoll3 + " " + diceRoll4 + " " + diceRoll5);
    }
    
    else {
      System.out.println("Enter a 5 digit number pertaining to the 5 numbers you want to roll (1-6):");
      userNumbers = myScanner.nextInt();
      diceRoll1 = (int)userNumbers / 10000;
      if (diceRoll1 < 1 || diceRoll1 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll1 * 10000);
      diceRoll2 = (int)userNumbers / 1000;
      if (diceRoll2 < 1 || diceRoll2 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll2 * 1000);
      diceRoll3 = (int)userNumbers / 100;
      if (diceRoll3 < 1 || diceRoll3 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll3 * 100);
      diceRoll4 = (int)userNumbers / 10;
      if (diceRoll4 < 1 || diceRoll4 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll4 * 10);
      diceRoll5 = (int)userNumbers;
      if (diceRoll5 < 1 || diceRoll5 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      }
    
    
    if (diceRoll1 == 1) {
      onesCounter += 1;
    }
    if (diceRoll1 == 2) {
      twosCounter += 1;
    }
    if (diceRoll1 == 3) {
      threesCounter += 1;
    }
    if (diceRoll1 == 4) {
      foursCounter += 1;
    }
    if (diceRoll1 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll1 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll2 == 1) {
      onesCounter += 1;
    }
    if (diceRoll2 == 2) {
      twosCounter += 1;
    }
    if (diceRoll2 == 3) {
      threesCounter += 1;
    }
    if (diceRoll2 == 4) {
      foursCounter += 1;
    }
    if (diceRoll2 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll2 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll3 == 1) {
      onesCounter += 1;
    }
    if (diceRoll3 == 2) {
      twosCounter += 1;
    }
    if (diceRoll3 == 3) {
      threesCounter += 1;
    }
    if (diceRoll3 == 4) {
      foursCounter += 1;
    }
    if (diceRoll3 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll3 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll4 == 1) {
      onesCounter += 1;
    }
    if (diceRoll4 == 2) {
      twosCounter += 1;
    }
    if (diceRoll4 == 3) {
      threesCounter += 1;
    }
    if (diceRoll4 == 4) {
      foursCounter += 1;
    }
    if (diceRoll4 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll4 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll5 == 1) {
      onesCounter += 1;
    }
    if (diceRoll5 == 2) {
      twosCounter += 1;
    }
    if (diceRoll5 == 3) {
      threesCounter += 1;
    }
    if (diceRoll5 == 4) {
      foursCounter += 1;
    }
    if (diceRoll5 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll5 == 6) {
      sixesCounter += 1;
    }
  
    
    System.out.println("Ones: " + onesCounter + " Twos: " + twosCounter + " Threes: " + threesCounter + " Fours: " + foursCounter + " Fives: " + fivesCounter + " Sixes: " + sixesCounter);
    
    if (yahtzee && onesCounter == 5 || twosCounter == 5 || threesCounter == 5 || foursCounter == 5 || fivesCounter == 5 || sixesCounter == 5) {
      System.out.println("You rolled a Yahtzee!");
      lowerScore += 50;
      yahtzee = false;
    }
    
    else if (largeStraight && (onesCounter == 1 && twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1) || (twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1 && sixesCounter == 1)) {
      System.out.println("You rolled a large straight");
      lowerScore += 40;
      largeStraight = false;
    }
    
    else if (smallStraight && (onesCounter >= 1 && twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1) || (twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1) || (threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1 && sixesCounter >= 1)) {
      System.out.println("You rolled a small straight");
      lowerScore += 30;
      smallStraight = false;
    }
    
    else if (fullHouse && (onesCounter == 2 || twosCounter == 2 || threesCounter == 2 || foursCounter == 2 || fivesCounter == 2 || sixesCounter == 2) && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a full house");
      lowerScore += 25;
      fullHouse = false;
    }
    
    else if (fourOfAKind && (onesCounter == 4 || twosCounter == 4 || threesCounter == 4 || foursCounter == 4 || fivesCounter == 4 || sixesCounter == 4)) {
      System.out.println("You rolled a 4 of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      fourOfAKind = false;
    }
    
    else if (threeOfAKind && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a three of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      threeOfAKind = false;
    }
    
    else if (sixesCounter >= 1 && sixes) {
      System.out.println("You rolled sixes");
      upperScore += sixesCounter * 6;
      sixes = false;
    }
    
    else if (fivesCounter >= 1 && fives) {
      System.out.println("You rolled fives");
      upperScore += fivesCounter * 5;
      fives = false;
    }
    
    else if (foursCounter >= 1 && fours) {
      System.out.println("You rolled fours");
      upperScore += foursCounter * 4;
      fours = false;
    }
    
    else if (threesCounter >= 1 && threes) {
      System.out.println("You rolled threes");
      upperScore += threesCounter * 3;
      threes = false;
    }
    
    else if (twosCounter >= 1 && twos) {
      System.out.println("You rolled twos");
      upperScore += twosCounter * 2;
      twos = false;
    }
    
    else if (onesCounter >= 1 && aces) {
      System.out.println("You rolled Aces");
      upperScore += onesCounter;
      aces = false;
    }
    
    else if (chance) {
      System.out.println("You rolled a chance");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      chance = false;
    }
    
    else {
      if (yahtzee) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Yahtzee'.");
        yahtzee = false;
      }
      else if (largeStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Large Straight'.");
        largeStraight = false;
      }
      else if (smallStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Small Straight'.");
        smallStraight = false;
      }
      else if (fullHouse) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Full House'.");
        fullHouse = false;
      }
      else if (fourOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Four of a Kind'.");
        fourOfAKind = false;
      }
      else if (threeOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Three of a Kind'.");
        threeOfAKind = false;
      }
      else if (sixes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Sixes'.");
        sixes = false;
      }
      else if (fives) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fives'.");
        fives = false;
      }
      else if (fours) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fours'.");
        fours = false;
      }
      else if (threes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Threes'.");
        threes = false;
      }
      else if (twos) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Twos'.");
        twos = false;
      }
      else {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Aces'.");
        aces = false;
      }
    }
    
    
    
    if (upperScore > 63 && upperScoreUsed) {
      System.out.println("Your upper score is greater than 63. You get a 35 point bonus");
      upperScore += 35;
      upperScoreUsed = false;
    }
    
    totalScore = upperScore + lowerScore;
    System.out.println("Upper score: " + upperScore + " Lower score: " + lowerScore + " Total Score: " + totalScore);
    
    onesCounter = 0;
    twosCounter = 0;
    threesCounter = 0;
    foursCounter = 0;
    fivesCounter = 0;
    sixesCounter = 0;
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    System.out.println("Roll 8. Do you want to generate random numbers or create your own? Enter '1' for random, enter another number to create your own:");
    int startRandomOrCYO8 = myScanner.nextInt();
    
    
    if (startRandomOrCYO8 == 1) {
      diceRoll1 = (int)(Math.random() * 6) + 1;
      diceRoll2 = (int)(Math.random() * 6) + 1;
      diceRoll3 = (int)(Math.random() * 6) + 1;
      diceRoll4 = (int)(Math.random() * 6) + 1;
      diceRoll5 = (int)(Math.random() * 6) + 1;
      System.out.println("You rolled " + diceRoll1 + " " + diceRoll2 + " " + diceRoll3 + " " + diceRoll4 + " " + diceRoll5);
    }
    
    else {
      System.out.println("Enter a 5 digit number pertaining to the 5 numbers you want to roll (1-6):");
      userNumbers = myScanner.nextInt();
      diceRoll1 = (int)userNumbers / 10000;
      if (diceRoll1 < 1 || diceRoll1 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll1 * 10000);
      diceRoll2 = (int)userNumbers / 1000;
      if (diceRoll2 < 1 || diceRoll2 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll2 * 1000);
      diceRoll3 = (int)userNumbers / 100;
      if (diceRoll3 < 1 || diceRoll3 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll3 * 100);
      diceRoll4 = (int)userNumbers / 10;
      if (diceRoll4 < 1 || diceRoll4 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll4 * 10);
      diceRoll5 = (int)userNumbers;
      if (diceRoll5 < 1 || diceRoll5 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      }
    
    
    if (diceRoll1 == 1) {
      onesCounter += 1;
    }
    if (diceRoll1 == 2) {
      twosCounter += 1;
    }
    if (diceRoll1 == 3) {
      threesCounter += 1;
    }
    if (diceRoll1 == 4) {
      foursCounter += 1;
    }
    if (diceRoll1 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll1 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll2 == 1) {
      onesCounter += 1;
    }
    if (diceRoll2 == 2) {
      twosCounter += 1;
    }
    if (diceRoll2 == 3) {
      threesCounter += 1;
    }
    if (diceRoll2 == 4) {
      foursCounter += 1;
    }
    if (diceRoll2 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll2 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll3 == 1) {
      onesCounter += 1;
    }
    if (diceRoll3 == 2) {
      twosCounter += 1;
    }
    if (diceRoll3 == 3) {
      threesCounter += 1;
    }
    if (diceRoll3 == 4) {
      foursCounter += 1;
    }
    if (diceRoll3 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll3 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll4 == 1) {
      onesCounter += 1;
    }
    if (diceRoll4 == 2) {
      twosCounter += 1;
    }
    if (diceRoll4 == 3) {
      threesCounter += 1;
    }
    if (diceRoll4 == 4) {
      foursCounter += 1;
    }
    if (diceRoll4 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll4 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll5 == 1) {
      onesCounter += 1;
    }
    if (diceRoll5 == 2) {
      twosCounter += 1;
    }
    if (diceRoll5 == 3) {
      threesCounter += 1;
    }
    if (diceRoll5 == 4) {
      foursCounter += 1;
    }
    if (diceRoll5 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll5 == 6) {
      sixesCounter += 1;
    }
  
    
    System.out.println("Ones: " + onesCounter + " Twos: " + twosCounter + " Threes: " + threesCounter + " Fours: " + foursCounter + " Fives: " + fivesCounter + " Sixes: " + sixesCounter);
    
    if (yahtzee && onesCounter == 5 || twosCounter == 5 || threesCounter == 5 || foursCounter == 5 || fivesCounter == 5 || sixesCounter == 5) {
      System.out.println("You rolled a Yahtzee!");
      lowerScore += 50;
      yahtzee = false;
    }
    
    else if (largeStraight && (onesCounter == 1 && twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1) || (twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1 && sixesCounter == 1)) {
      System.out.println("You rolled a large straight");
      lowerScore += 40;
      largeStraight = false;
    }
    
    else if (smallStraight && (onesCounter >= 1 && twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1) || (twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1) || (threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1 && sixesCounter >= 1)) {
      System.out.println("You rolled a small straight");
      lowerScore += 30;
      smallStraight = false;
    }
    
    else if (fullHouse && (onesCounter == 2 || twosCounter == 2 || threesCounter == 2 || foursCounter == 2 || fivesCounter == 2 || sixesCounter == 2) && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a full house");
      lowerScore += 25;
      fullHouse = false;
    }
    
    else if (fourOfAKind && (onesCounter == 4 || twosCounter == 4 || threesCounter == 4 || foursCounter == 4 || fivesCounter == 4 || sixesCounter == 4)) {
      System.out.println("You rolled a 4 of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      fourOfAKind = false;
    }
    
    else if (threeOfAKind && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a three of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      threeOfAKind = false;
    }
    
    else if (sixesCounter >= 1 && sixes) {
      System.out.println("You rolled sixes");
      upperScore += sixesCounter * 6;
      sixes = false;
    }
    
    else if (fivesCounter >= 1 && fives) {
      System.out.println("You rolled fives");
      upperScore += fivesCounter * 5;
      fives = false;
    }
    
    else if (foursCounter >= 1 && fours) {
      System.out.println("You rolled fours");
      upperScore += foursCounter * 4;
      fours = false;
    }
    
    else if (threesCounter >= 1 && threes) {
      System.out.println("You rolled threes");
      upperScore += threesCounter * 3;
      threes = false;
    }
    
    else if (twosCounter >= 1 && twos) {
      System.out.println("You rolled twos");
      upperScore += twosCounter * 2;
      twos = false;
    }
    
    else if (onesCounter >= 1 && aces) {
      System.out.println("You rolled Aces");
      upperScore += onesCounter;
      aces = false;
    }
    
    else if (chance) {
      System.out.println("You rolled a chance");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      chance = false;
    }
    
    else {
      if (yahtzee) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Yahtzee'.");
        yahtzee = false;
      }
      else if (largeStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Large Straight'.");
        largeStraight = false;
      }
      else if (smallStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Small Straight'.");
        smallStraight = false;
      }
      else if (fullHouse) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Full House'.");
        fullHouse = false;
      }
      else if (fourOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Four of a Kind'.");
        fourOfAKind = false;
      }
      else if (threeOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Three of a Kind'.");
        threeOfAKind = false;
      }
      else if (sixes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Sixes'.");
        sixes = false;
      }
      else if (fives) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fives'.");
        fives = false;
      }
      else if (fours) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fours'.");
        fours = false;
      }
      else if (threes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Threes'.");
        threes = false;
      }
      else if (twos) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Twos'.");
        twos = false;
      }
      else {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Aces'.");
        aces = false;
      }
    }
    
    
    if (upperScore > 63 && upperScoreUsed) {
      System.out.println("Your upper score is greater than 63. You get a 35 point bonus");
      upperScore += 35;
      upperScoreUsed = false;
    }
    
    totalScore = upperScore + lowerScore;
    System.out.println("Upper score: " + upperScore + " Lower score: " + lowerScore + " Total Score: " + totalScore);
    
    onesCounter = 0;
    twosCounter = 0;
    threesCounter = 0;
    foursCounter = 0;
    fivesCounter = 0;
    sixesCounter = 0;
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    System.out.println("Roll 9. Do you want to generate random numbers or create your own? Enter '1' for random, enter another number to create your own:");
    int startRandomOrCYO9 = myScanner.nextInt();
    
    
    if (startRandomOrCYO9 == 1) {
      diceRoll1 = (int)(Math.random() * 6) + 1;
      diceRoll2 = (int)(Math.random() * 6) + 1;
      diceRoll3 = (int)(Math.random() * 6) + 1;
      diceRoll4 = (int)(Math.random() * 6) + 1;
      diceRoll5 = (int)(Math.random() * 6) + 1;
      System.out.println("You rolled " + diceRoll1 + " " + diceRoll2 + " " + diceRoll3 + " " + diceRoll4 + " " + diceRoll5);
    }
    
    else {
      System.out.println("Enter a 5 digit number pertaining to the 5 numbers you want to roll (1-6):");
      userNumbers = myScanner.nextInt();
      diceRoll1 = (int)userNumbers / 10000;
      if (diceRoll1 < 1 || diceRoll1 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll1 * 10000);
      diceRoll2 = (int)userNumbers / 1000;
      if (diceRoll2 < 1 || diceRoll2 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll2 * 1000);
      diceRoll3 = (int)userNumbers / 100;
      if (diceRoll3 < 1 || diceRoll3 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll3 * 100);
      diceRoll4 = (int)userNumbers / 10;
      if (diceRoll4 < 1 || diceRoll4 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll4 * 10);
      diceRoll5 = (int)userNumbers;
      if (diceRoll5 < 1 || diceRoll5 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      }
    
    
    if (diceRoll1 == 1) {
      onesCounter += 1;
    }
    if (diceRoll1 == 2) {
      twosCounter += 1;
    }
    if (diceRoll1 == 3) {
      threesCounter += 1;
    }
    if (diceRoll1 == 4) {
      foursCounter += 1;
    }
    if (diceRoll1 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll1 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll2 == 1) {
      onesCounter += 1;
    }
    if (diceRoll2 == 2) {
      twosCounter += 1;
    }
    if (diceRoll2 == 3) {
      threesCounter += 1;
    }
    if (diceRoll2 == 4) {
      foursCounter += 1;
    }
    if (diceRoll2 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll2 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll3 == 1) {
      onesCounter += 1;
    }
    if (diceRoll3 == 2) {
      twosCounter += 1;
    }
    if (diceRoll3 == 3) {
      threesCounter += 1;
    }
    if (diceRoll3 == 4) {
      foursCounter += 1;
    }
    if (diceRoll3 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll3 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll4 == 1) {
      onesCounter += 1;
    }
    if (diceRoll4 == 2) {
      twosCounter += 1;
    }
    if (diceRoll4 == 3) {
      threesCounter += 1;
    }
    if (diceRoll4 == 4) {
      foursCounter += 1;
    }
    if (diceRoll4 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll4 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll5 == 1) {
      onesCounter += 1;
    }
    if (diceRoll5 == 2) {
      twosCounter += 1;
    }
    if (diceRoll5 == 3) {
      threesCounter += 1;
    }
    if (diceRoll5 == 4) {
      foursCounter += 1;
    }
    if (diceRoll5 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll5 == 6) {
      sixesCounter += 1;
    }
  
    
    System.out.println("Ones: " + onesCounter + " Twos: " + twosCounter + " Threes: " + threesCounter + " Fours: " + foursCounter + " Fives: " + fivesCounter + " Sixes: " + sixesCounter);
    
    if (yahtzee && onesCounter == 5 || twosCounter == 5 || threesCounter == 5 || foursCounter == 5 || fivesCounter == 5 || sixesCounter == 5) {
      System.out.println("You rolled a Yahtzee!");
      lowerScore += 50;
      yahtzee = false;
    }
    
    else if (largeStraight && (onesCounter == 1 && twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1) || (twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1 && sixesCounter == 1)) {
      System.out.println("You rolled a large straight");
      lowerScore += 40;
      largeStraight = false;
    }
    
    else if (smallStraight && (onesCounter >= 1 && twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1) || (twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1) || (threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1 && sixesCounter >= 1)) {
      System.out.println("You rolled a small straight");
      lowerScore += 30;
      smallStraight = false;
    }
    
    else if (fullHouse && (onesCounter == 2 || twosCounter == 2 || threesCounter == 2 || foursCounter == 2 || fivesCounter == 2 || sixesCounter == 2) && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a full house");
      lowerScore += 25;
      fullHouse = false;
    }
    
    else if (fourOfAKind && (onesCounter == 4 || twosCounter == 4 || threesCounter == 4 || foursCounter == 4 || fivesCounter == 4 || sixesCounter == 4)) {
      System.out.println("You rolled a 4 of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      fourOfAKind = false;
    }
    
    else if (threeOfAKind && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a three of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      threeOfAKind = false;
    }
    
    else if (sixesCounter >= 1 && sixes) {
      System.out.println("You rolled sixes");
      upperScore += sixesCounter * 6;
      sixes = false;
    }
    
    else if (fivesCounter >= 1 && fives) {
      System.out.println("You rolled fives");
      upperScore += fivesCounter * 5;
      fives = false;
    }
    
    else if (foursCounter >= 1 && fours) {
      System.out.println("You rolled fours");
      upperScore += foursCounter * 4;
      fours = false;
    }
    
    else if (threesCounter >= 1 && threes) {
      System.out.println("You rolled threes");
      upperScore += threesCounter * 3;
      threes = false;
    }
    
    else if (twosCounter >= 1 && twos) {
      System.out.println("You rolled twos");
      upperScore += twosCounter * 2;
      twos = false;
    }
    
    else if (onesCounter >= 1 && aces) {
      System.out.println("You rolled Aces");
      upperScore += onesCounter;
      aces = false;
    }
    
    else if (chance) {
      System.out.println("You rolled a chance");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      chance = false;
    }
    
    else {
      if (yahtzee) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Yahtzee'.");
        yahtzee = false;
      }
      else if (largeStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Large Straight'.");
        largeStraight = false;
      }
      else if (smallStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Small Straight'.");
        smallStraight = false;
      }
      else if (fullHouse) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Full House'.");
        fullHouse = false;
      }
      else if (fourOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Four of a Kind'.");
        fourOfAKind = false;
      }
      else if (threeOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Three of a Kind'.");
        threeOfAKind = false;
      }
      else if (sixes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Sixes'.");
        sixes = false;
      }
      else if (fives) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fives'.");
        fives = false;
      }
      else if (fours) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fours'.");
        fours = false;
      }
      else if (threes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Threes'.");
        threes = false;
      }
      else if (twos) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Twos'.");
        twos = false;
      }
      else {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Aces'.");
        aces = false;
      }
    }
    
    
    
    if (upperScore > 63 && upperScoreUsed) {
      System.out.println("Your upper score is greater than 63. You get a 35 point bonus");
      upperScore += 35;
      upperScoreUsed = false;
    }
    
    totalScore = upperScore + lowerScore;
    System.out.println("Upper score: " + upperScore + " Lower score: " + lowerScore + " Total Score: " + totalScore);
    
    onesCounter = 0;
    twosCounter = 0;
    threesCounter = 0;
    foursCounter = 0;
    fivesCounter = 0;
    sixesCounter = 0;
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    System.out.println("Roll 10. Do you want to generate random numbers or create your own? Enter '1' for random, enter another number to create your own:");
    int startRandomOrCYO10 = myScanner.nextInt();
    
    
    if (startRandomOrCYO10 == 1) {
      diceRoll1 = (int)(Math.random() * 6) + 1;
      diceRoll2 = (int)(Math.random() * 6) + 1;
      diceRoll3 = (int)(Math.random() * 6) + 1;
      diceRoll4 = (int)(Math.random() * 6) + 1;
      diceRoll5 = (int)(Math.random() * 6) + 1;
      System.out.println("You rolled " + diceRoll1 + " " + diceRoll2 + " " + diceRoll3 + " " + diceRoll4 + " " + diceRoll5);
    }
    
    else {
      System.out.println("Enter a 5 digit number pertaining to the 5 numbers you want to roll (1-6):");
      userNumbers = myScanner.nextInt();
      diceRoll1 = (int)userNumbers / 10000;
      if (diceRoll1 < 1 || diceRoll1 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll1 * 10000);
      diceRoll2 = (int)userNumbers / 1000;
      if (diceRoll2 < 1 || diceRoll2 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll2 * 1000);
      diceRoll3 = (int)userNumbers / 100;
      if (diceRoll3 < 1 || diceRoll3 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll3 * 100);
      diceRoll4 = (int)userNumbers / 10;
      if (diceRoll4 < 1 || diceRoll4 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll4 * 10);
      diceRoll5 = (int)userNumbers;
      if (diceRoll5 < 1 || diceRoll5 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      }
    
    
    if (diceRoll1 == 1) {
      onesCounter += 1;
    }
    if (diceRoll1 == 2) {
      twosCounter += 1;
    }
    if (diceRoll1 == 3) {
      threesCounter += 1;
    }
    if (diceRoll1 == 4) {
      foursCounter += 1;
    }
    if (diceRoll1 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll1 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll2 == 1) {
      onesCounter += 1;
    }
    if (diceRoll2 == 2) {
      twosCounter += 1;
    }
    if (diceRoll2 == 3) {
      threesCounter += 1;
    }
    if (diceRoll2 == 4) {
      foursCounter += 1;
    }
    if (diceRoll2 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll2 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll3 == 1) {
      onesCounter += 1;
    }
    if (diceRoll3 == 2) {
      twosCounter += 1;
    }
    if (diceRoll3 == 3) {
      threesCounter += 1;
    }
    if (diceRoll3 == 4) {
      foursCounter += 1;
    }
    if (diceRoll3 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll3 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll4 == 1) {
      onesCounter += 1;
    }
    if (diceRoll4 == 2) {
      twosCounter += 1;
    }
    if (diceRoll4 == 3) {
      threesCounter += 1;
    }
    if (diceRoll4 == 4) {
      foursCounter += 1;
    }
    if (diceRoll4 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll4 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll5 == 1) {
      onesCounter += 1;
    }
    if (diceRoll5 == 2) {
      twosCounter += 1;
    }
    if (diceRoll5 == 3) {
      threesCounter += 1;
    }
    if (diceRoll5 == 4) {
      foursCounter += 1;
    }
    if (diceRoll5 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll5 == 6) {
      sixesCounter += 1;
    }
  
    
    System.out.println("Ones: " + onesCounter + " Twos: " + twosCounter + " Threes: " + threesCounter + " Fours: " + foursCounter + " Fives: " + fivesCounter + " Sixes: " + sixesCounter);
    
    if (yahtzee && onesCounter == 5 || twosCounter == 5 || threesCounter == 5 || foursCounter == 5 || fivesCounter == 5 || sixesCounter == 5) {
      System.out.println("You rolled a Yahtzee!");
      lowerScore += 50;
      yahtzee = false;
    }
    
    else if (largeStraight && (onesCounter == 1 && twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1) || (twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1 && sixesCounter == 1)) {
      System.out.println("You rolled a large straight");
      lowerScore += 40;
      largeStraight = false;
    }
    
    else if (smallStraight && (onesCounter >= 1 && twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1) || (twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1) || (threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1 && sixesCounter >= 1)) {
      System.out.println("You rolled a small straight");
      lowerScore += 30;
      smallStraight = false;
    }
    
    else if (fullHouse && (onesCounter == 2 || twosCounter == 2 || threesCounter == 2 || foursCounter == 2 || fivesCounter == 2 || sixesCounter == 2) && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a full house");
      lowerScore += 25;
      fullHouse = false;
    }
    
    else if (fourOfAKind && (onesCounter == 4 || twosCounter == 4 || threesCounter == 4 || foursCounter == 4 || fivesCounter == 4 || sixesCounter == 4)) {
      System.out.println("You rolled a 4 of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      fourOfAKind = false;
    }
    
    else if (threeOfAKind && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a three of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      threeOfAKind = false;
    }
    
    else if (sixesCounter >= 1 && sixes) {
      System.out.println("You rolled sixes");
      upperScore += sixesCounter * 6;
      sixes = false;
    }
    
    else if (fivesCounter >= 1 && fives) {
      System.out.println("You rolled fives");
      upperScore += fivesCounter * 5;
      fives = false;
    }
    
    else if (foursCounter >= 1 && fours) {
      System.out.println("You rolled fours");
      upperScore += foursCounter * 4;
      fours = false;
    }
    
    else if (threesCounter >= 1 && threes) {
      System.out.println("You rolled threes");
      upperScore += threesCounter * 3;
      threes = false;
    }
    
    else if (twosCounter >= 1 && twos) {
      System.out.println("You rolled twos");
      upperScore += twosCounter * 2;
      twos = false;
    }
    
    else if (onesCounter >= 1 && aces) {
      System.out.println("You rolled Aces");
      upperScore += onesCounter;
      aces = false;
    }
    
    else if (chance) {
      System.out.println("You rolled a chance");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      chance = false;
    }
    
    else {
      if (yahtzee) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Yahtzee'.");
        yahtzee = false;
      }
      else if (largeStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Large Straight'.");
        largeStraight = false;
      }
      else if (smallStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Small Straight'.");
        smallStraight = false;
      }
      else if (fullHouse) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Full House'.");
        fullHouse = false;
      }
      else if (fourOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Four of a Kind'.");
        fourOfAKind = false;
      }
      else if (threeOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Three of a Kind'.");
        threeOfAKind = false;
      }
      else if (sixes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Sixes'.");
        sixes = false;
      }
      else if (fives) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fives'.");
        fives = false;
      }
      else if (fours) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fours'.");
        fours = false;
      }
      else if (threes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Threes'.");
        threes = false;
      }
      else if (twos) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Twos'.");
        twos = false;
      }
      else {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Aces'.");
        aces = false;
      }
    }
    
    
    
    if (upperScore > 63 && upperScoreUsed) {
      System.out.println("Your upper score is greater than 63. You get a 35 point bonus");
      upperScore += 35;
      upperScoreUsed = false;
    }
    
    totalScore = upperScore + lowerScore;
    System.out.println("Upper score: " + upperScore + " Lower score: " + lowerScore + " Total Score: " + totalScore);
    
    onesCounter = 0;
    twosCounter = 0;
    threesCounter = 0;
    foursCounter = 0;
    fivesCounter = 0;
    sixesCounter = 0;
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    System.out.println("Roll 11. Do you want to generate random numbers or create your own? Enter '1' for random, enter another number to create your own:");
    int startRandomOrCYO11 = myScanner.nextInt();
    
    
    if (startRandomOrCYO11 == 1) {
      diceRoll1 = (int)(Math.random() * 6) + 1;
      diceRoll2 = (int)(Math.random() * 6) + 1;
      diceRoll3 = (int)(Math.random() * 6) + 1;
      diceRoll4 = (int)(Math.random() * 6) + 1;
      diceRoll5 = (int)(Math.random() * 6) + 1;
      System.out.println("You rolled " + diceRoll1 + " " + diceRoll2 + " " + diceRoll3 + " " + diceRoll4 + " " + diceRoll5);
    }
    
    else {
      System.out.println("Enter a 5 digit number pertaining to the 5 numbers you want to roll (1-6):");
      userNumbers = myScanner.nextInt();
      diceRoll1 = (int)userNumbers / 10000;
      if (diceRoll1 < 1 || diceRoll1 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll1 * 10000);
      diceRoll2 = (int)userNumbers / 1000;
      if (diceRoll2 < 1 || diceRoll2 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll2 * 1000);
      diceRoll3 = (int)userNumbers / 100;
      if (diceRoll3 < 1 || diceRoll3 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll3 * 100);
      diceRoll4 = (int)userNumbers / 10;
      if (diceRoll4 < 1 || diceRoll4 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll4 * 10);
      diceRoll5 = (int)userNumbers;
      if (diceRoll5 < 1 || diceRoll5 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      }
    
    
    if (diceRoll1 == 1) {
      onesCounter += 1;
    }
    if (diceRoll1 == 2) {
      twosCounter += 1;
    }
    if (diceRoll1 == 3) {
      threesCounter += 1;
    }
    if (diceRoll1 == 4) {
      foursCounter += 1;
    }
    if (diceRoll1 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll1 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll2 == 1) {
      onesCounter += 1;
    }
    if (diceRoll2 == 2) {
      twosCounter += 1;
    }
    if (diceRoll2 == 3) {
      threesCounter += 1;
    }
    if (diceRoll2 == 4) {
      foursCounter += 1;
    }
    if (diceRoll2 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll2 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll3 == 1) {
      onesCounter += 1;
    }
    if (diceRoll3 == 2) {
      twosCounter += 1;
    }
    if (diceRoll3 == 3) {
      threesCounter += 1;
    }
    if (diceRoll3 == 4) {
      foursCounter += 1;
    }
    if (diceRoll3 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll3 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll4 == 1) {
      onesCounter += 1;
    }
    if (diceRoll4 == 2) {
      twosCounter += 1;
    }
    if (diceRoll4 == 3) {
      threesCounter += 1;
    }
    if (diceRoll4 == 4) {
      foursCounter += 1;
    }
    if (diceRoll4 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll4 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll5 == 1) {
      onesCounter += 1;
    }
    if (diceRoll5 == 2) {
      twosCounter += 1;
    }
    if (diceRoll5 == 3) {
      threesCounter += 1;
    }
    if (diceRoll5 == 4) {
      foursCounter += 1;
    }
    if (diceRoll5 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll5 == 6) {
      sixesCounter += 1;
    }
  
    
    System.out.println("Ones: " + onesCounter + " Twos: " + twosCounter + " Threes: " + threesCounter + " Fours: " + foursCounter + " Fives: " + fivesCounter + " Sixes: " + sixesCounter);
    
    if (yahtzee && onesCounter == 5 || twosCounter == 5 || threesCounter == 5 || foursCounter == 5 || fivesCounter == 5 || sixesCounter == 5) {
      System.out.println("You rolled a Yahtzee!");
      lowerScore += 50;
      yahtzee = false;
    }
    
    else if (largeStraight && (onesCounter == 1 && twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1) || (twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1 && sixesCounter == 1)) {
      System.out.println("You rolled a large straight");
      lowerScore += 40;
      largeStraight = false;
    }
    
    else if (smallStraight && (onesCounter >= 1 && twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1) || (twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1) || (threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1 && sixesCounter >= 1)) {
      System.out.println("You rolled a small straight");
      lowerScore += 30;
      smallStraight = false;
    }
    
    else if (fullHouse && (onesCounter == 2 || twosCounter == 2 || threesCounter == 2 || foursCounter == 2 || fivesCounter == 2 || sixesCounter == 2) && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a full house");
      lowerScore += 25;
      fullHouse = false;
    }
    
    else if (fourOfAKind && (onesCounter == 4 || twosCounter == 4 || threesCounter == 4 || foursCounter == 4 || fivesCounter == 4 || sixesCounter == 4)) {
      System.out.println("You rolled a 4 of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      fourOfAKind = false;
    }
    
    else if (threeOfAKind && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a three of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      threeOfAKind = false;
    }
    
    else if (sixesCounter >= 1 && sixes) {
      System.out.println("You rolled sixes");
      upperScore += sixesCounter * 6;
      sixes = false;
    }
    
    else if (fivesCounter >= 1 && fives) {
      System.out.println("You rolled fives");
      upperScore += fivesCounter * 5;
      fives = false;
    }
    
    else if (foursCounter >= 1 && fours) {
      System.out.println("You rolled fours");
      upperScore += foursCounter * 4;
      fours = false;
    }
    
    else if (threesCounter >= 1 && threes) {
      System.out.println("You rolled threes");
      upperScore += threesCounter * 3;
      threes = false;
    }
    
    else if (twosCounter >= 1 && twos) {
      System.out.println("You rolled twos");
      upperScore += twosCounter * 2;
      twos = false;
    }
    
    else if (onesCounter >= 1 && aces) {
      System.out.println("You rolled Aces");
      upperScore += onesCounter;
      aces = false;
    }
    
    else if (chance) {
      System.out.println("You rolled a chance");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      chance = false;
    }
    
    else {
      if (yahtzee) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Yahtzee'.");
        yahtzee = false;
      }
      else if (largeStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Large Straight'.");
        largeStraight = false;
      }
      else if (smallStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Small Straight'.");
        smallStraight = false;
      }
      else if (fullHouse) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Full House'.");
        fullHouse = false;
      }
      else if (fourOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Four of a Kind'.");
        fourOfAKind = false;
      }
      else if (threeOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Three of a Kind'.");
        threeOfAKind = false;
      }
      else if (sixes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Sixes'.");
        sixes = false;
      }
      else if (fives) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fives'.");
        fives = false;
      }
      else if (fours) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fours'.");
        fours = false;
      }
      else if (threes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Threes'.");
        threes = false;
      }
      else if (twos) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Twos'.");
        twos = false;
      }
      else {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Aces'.");
        aces = false;
      }
    }
    
    
    
    if (upperScore > 63 && upperScoreUsed) {
      System.out.println("Your upper score is greater than 63. You get a 35 point bonus");
      upperScore += 35;
      upperScoreUsed = false;
    }
    
    totalScore = upperScore + lowerScore;
    System.out.println("Upper score: " + upperScore + " Lower score: " + lowerScore + " Total Score: " + totalScore);
    
    onesCounter = 0;
    twosCounter = 0;
    threesCounter = 0;
    foursCounter = 0;
    fivesCounter = 0;
    sixesCounter = 0;
    
    
    
    
    
    
    
    
    
    
    
    
    
    System.out.println("Roll 12. Do you want to generate random numbers or create your own? Enter '1' for random, enter another number to create your own:");
    int startRandomOrCYO12 = myScanner.nextInt();
    
    
    if (startRandomOrCYO12 == 1) {
      diceRoll1 = (int)(Math.random() * 6) + 1;
      diceRoll2 = (int)(Math.random() * 6) + 1;
      diceRoll3 = (int)(Math.random() * 6) + 1;
      diceRoll4 = (int)(Math.random() * 6) + 1;
      diceRoll5 = (int)(Math.random() * 6) + 1;
      System.out.println("You rolled " + diceRoll1 + " " + diceRoll2 + " " + diceRoll3 + " " + diceRoll4 + " " + diceRoll5);
    }
    
    else {
      System.out.println("Enter a 5 digit number pertaining to the 5 numbers you want to roll (1-6):");
      userNumbers = myScanner.nextInt();
      diceRoll1 = (int)userNumbers / 10000;
      if (diceRoll1 < 1 || diceRoll1 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll1 * 10000);
      diceRoll2 = (int)userNumbers / 1000;
      if (diceRoll2 < 1 || diceRoll2 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll2 * 1000);
      diceRoll3 = (int)userNumbers / 100;
      if (diceRoll3 < 1 || diceRoll3 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll3 * 100);
      diceRoll4 = (int)userNumbers / 10;
      if (diceRoll4 < 1 || diceRoll4 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll4 * 10);
      diceRoll5 = (int)userNumbers;
      if (diceRoll5 < 1 || diceRoll5 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      }
    
    
    if (diceRoll1 == 1) {
      onesCounter += 1;
    }
    if (diceRoll1 == 2) {
      twosCounter += 1;
    }
    if (diceRoll1 == 3) {
      threesCounter += 1;
    }
    if (diceRoll1 == 4) {
      foursCounter += 1;
    }
    if (diceRoll1 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll1 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll2 == 1) {
      onesCounter += 1;
    }
    if (diceRoll2 == 2) {
      twosCounter += 1;
    }
    if (diceRoll2 == 3) {
      threesCounter += 1;
    }
    if (diceRoll2 == 4) {
      foursCounter += 1;
    }
    if (diceRoll2 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll2 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll3 == 1) {
      onesCounter += 1;
    }
    if (diceRoll3 == 2) {
      twosCounter += 1;
    }
    if (diceRoll3 == 3) {
      threesCounter += 1;
    }
    if (diceRoll3 == 4) {
      foursCounter += 1;
    }
    if (diceRoll3 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll3 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll4 == 1) {
      onesCounter += 1;
    }
    if (diceRoll4 == 2) {
      twosCounter += 1;
    }
    if (diceRoll4 == 3) {
      threesCounter += 1;
    }
    if (diceRoll4 == 4) {
      foursCounter += 1;
    }
    if (diceRoll4 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll4 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll5 == 1) {
      onesCounter += 1;
    }
    if (diceRoll5 == 2) {
      twosCounter += 1;
    }
    if (diceRoll5 == 3) {
      threesCounter += 1;
    }
    if (diceRoll5 == 4) {
      foursCounter += 1;
    }
    if (diceRoll5 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll5 == 6) {
      sixesCounter += 1;
    }
  
    
    System.out.println("Ones: " + onesCounter + " Twos: " + twosCounter + " Threes: " + threesCounter + " Fours: " + foursCounter + " Fives: " + fivesCounter + " Sixes: " + sixesCounter);
    
    if (yahtzee && onesCounter == 5 || twosCounter == 5 || threesCounter == 5 || foursCounter == 5 || fivesCounter == 5 || sixesCounter == 5) {
      System.out.println("You rolled a Yahtzee!");
      lowerScore += 50;
      yahtzee = false;
    }
    
    else if (largeStraight && (onesCounter == 1 && twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1) || (twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1 && sixesCounter == 1)) {
      System.out.println("You rolled a large straight");
      lowerScore += 40;
      largeStraight = false;
    }
    
    else if (smallStraight && (onesCounter >= 1 && twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1) || (twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1) || (threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1 && sixesCounter >= 1)) {
      System.out.println("You rolled a small straight");
      lowerScore += 30;
      smallStraight = false;
    }
    
    else if (fullHouse && (onesCounter == 2 || twosCounter == 2 || threesCounter == 2 || foursCounter == 2 || fivesCounter == 2 || sixesCounter == 2) && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a full house");
      lowerScore += 25;
      fullHouse = false;
    }
    
    else if (fourOfAKind && (onesCounter == 4 || twosCounter == 4 || threesCounter == 4 || foursCounter == 4 || fivesCounter == 4 || sixesCounter == 4)) {
      System.out.println("You rolled a 4 of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      fourOfAKind = false;
    }
    
    else if (threeOfAKind && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a three of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      threeOfAKind = false;
    }
    
    else if (sixesCounter >= 1 && sixes) {
      System.out.println("You rolled sixes");
      upperScore += sixesCounter * 6;
      sixes = false;
    }
    
    else if (fivesCounter >= 1 && fives) {
      System.out.println("You rolled fives");
      upperScore += fivesCounter * 5;
      fives = false;
    }
    
    else if (foursCounter >= 1 && fours) {
      System.out.println("You rolled fours");
      upperScore += foursCounter * 4;
      fours = false;
    }
    
    else if (threesCounter >= 1 && threes) {
      System.out.println("You rolled threes");
      upperScore += threesCounter * 3;
      threes = false;
    }
    
    else if (twosCounter >= 1 && twos) {
      System.out.println("You rolled twos");
      upperScore += twosCounter * 2;
      twos = false;
    }
    
    else if (onesCounter >= 1 && aces) {
      System.out.println("You rolled Aces");
      upperScore += onesCounter;
      aces = false;
    }
    
    else if (chance) {
      System.out.println("You rolled a chance");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      chance = false;
    }
    
    else {
      if (yahtzee) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Yahtzee'.");
        yahtzee = false;
      }
      else if (largeStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Large Straight'.");
        largeStraight = false;
      }
      else if (smallStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Small Straight'.");
        smallStraight = false;
      }
      else if (fullHouse) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Full House'.");
        fullHouse = false;
      }
      else if (fourOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Four of a Kind'.");
        fourOfAKind = false;
      }
      else if (threeOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Three of a Kind'.");
        threeOfAKind = false;
      }
      else if (sixes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Sixes'.");
        sixes = false;
      }
      else if (fives) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fives'.");
        fives = false;
      }
      else if (fours) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fours'.");
        fours = false;
      }
      else if (threes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Threes'.");
        threes = false;
      }
      else if (twos) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Twos'.");
        twos = false;
      }
      else {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Aces'.");
        aces = false;
      }
    }
    
    
    
    if (upperScore > 63 && upperScoreUsed) {
      System.out.println("Your upper score is greater than 63. You get a 35 point bonus");
      upperScore += 35;
      upperScoreUsed = false;
    }
    
    totalScore = upperScore + lowerScore;
    System.out.println("Upper score: " + upperScore + " Lower score: " + lowerScore + " Total Score: " + totalScore);
    
    onesCounter = 0;
    twosCounter = 0;
    threesCounter = 0;
    foursCounter = 0;
    fivesCounter = 0;
    sixesCounter = 0;
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    System.out.println("Roll 13. Do you want to generate random numbers or create your own? Enter '1' for random, enter another number to create your own:");
    int startRandomOrCYO13 = myScanner.nextInt();
    
    
    if (startRandomOrCYO13 == 1) {
      diceRoll1 = (int)(Math.random() * 6) + 1;
      diceRoll2 = (int)(Math.random() * 6) + 1;
      diceRoll3 = (int)(Math.random() * 6) + 1;
      diceRoll4 = (int)(Math.random() * 6) + 1;
      diceRoll5 = (int)(Math.random() * 6) + 1;
      System.out.println("You rolled " + diceRoll1 + " " + diceRoll2 + " " + diceRoll3 + " " + diceRoll4 + " " + diceRoll5);
    }
    
    else {
      System.out.println("Enter a 5 digit number pertaining to the 5 numbers you want to roll (1-6):");
      userNumbers = myScanner.nextInt();
      diceRoll1 = (int)userNumbers / 10000;
      if (diceRoll1 < 1 || diceRoll1 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll1 * 10000);
      diceRoll2 = (int)userNumbers / 1000;
      if (diceRoll2 < 1 || diceRoll2 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll2 * 1000);
      diceRoll3 = (int)userNumbers / 100;
      if (diceRoll3 < 1 || diceRoll3 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll3 * 100);
      diceRoll4 = (int)userNumbers / 10;
      if (diceRoll4 < 1 || diceRoll4 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      userNumbers = userNumbers - (diceRoll4 * 10);
      diceRoll5 = (int)userNumbers;
      if (diceRoll5 < 1 || diceRoll5 > 6) {
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0);
      }
      }
    
    
    if (diceRoll1 == 1) {
      onesCounter += 1;
    }
    if (diceRoll1 == 2) {
      twosCounter += 1;
    }
    if (diceRoll1 == 3) {
      threesCounter += 1;
    }
    if (diceRoll1 == 4) {
      foursCounter += 1;
    }
    if (diceRoll1 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll1 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll2 == 1) {
      onesCounter += 1;
    }
    if (diceRoll2 == 2) {
      twosCounter += 1;
    }
    if (diceRoll2 == 3) {
      threesCounter += 1;
    }
    if (diceRoll2 == 4) {
      foursCounter += 1;
    }
    if (diceRoll2 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll2 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll3 == 1) {
      onesCounter += 1;
    }
    if (diceRoll3 == 2) {
      twosCounter += 1;
    }
    if (diceRoll3 == 3) {
      threesCounter += 1;
    }
    if (diceRoll3 == 4) {
      foursCounter += 1;
    }
    if (diceRoll3 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll3 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll4 == 1) {
      onesCounter += 1;
    }
    if (diceRoll4 == 2) {
      twosCounter += 1;
    }
    if (diceRoll4 == 3) {
      threesCounter += 1;
    }
    if (diceRoll4 == 4) {
      foursCounter += 1;
    }
    if (diceRoll4 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll4 == 6) {
      sixesCounter += 1;
    }
    
    if (diceRoll5 == 1) {
      onesCounter += 1;
    }
    if (diceRoll5 == 2) {
      twosCounter += 1;
    }
    if (diceRoll5 == 3) {
      threesCounter += 1;
    }
    if (diceRoll5 == 4) {
      foursCounter += 1;
    }
    if (diceRoll5 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll5 == 6) {
      sixesCounter += 1;
    }
  
    
    System.out.println("Ones: " + onesCounter + " Twos: " + twosCounter + " Threes: " + threesCounter + " Fours: " + foursCounter + " Fives: " + fivesCounter + " Sixes: " + sixesCounter);
    
    if (yahtzee && onesCounter == 5 || twosCounter == 5 || threesCounter == 5 || foursCounter == 5 || fivesCounter == 5 || sixesCounter == 5) {
      System.out.println("You rolled a Yahtzee!");
      lowerScore += 50;
      yahtzee = false;
    }
    
    else if (largeStraight && (onesCounter == 1 && twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1) || (twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1 && sixesCounter == 1)) {
      System.out.println("You rolled a large straight");
      lowerScore += 40;
      largeStraight = false;
    }
    
    else if (smallStraight && (onesCounter >= 1 && twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1) || (twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1) || (threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1 && sixesCounter >= 1)) {
      System.out.println("You rolled a small straight");
      lowerScore += 30;
      smallStraight = false;
    }
    
    else if (fullHouse && (onesCounter == 2 || twosCounter == 2 || threesCounter == 2 || foursCounter == 2 || fivesCounter == 2 || sixesCounter == 2) && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a full house");
      lowerScore += 25;
      fullHouse = false;
    }
    
    else if (fourOfAKind && (onesCounter == 4 || twosCounter == 4 || threesCounter == 4 || foursCounter == 4 || fivesCounter == 4 || sixesCounter == 4)) {
      System.out.println("You rolled a 4 of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      fourOfAKind = false;
    }
    
    else if (threeOfAKind && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a three of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      threeOfAKind = false;
    }
    
    else if (sixesCounter >= 1 && sixes) {
      System.out.println("You rolled sixes");
      upperScore += sixesCounter * 6;
      sixes = false;
    }
    
    else if (fivesCounter >= 1 && fives) {
      System.out.println("You rolled fives");
      upperScore += fivesCounter * 5;
      fives = false;
    }
    
    else if (foursCounter >= 1 && fours) {
      System.out.println("You rolled fours");
      upperScore += foursCounter * 4;
      fours = false;
    }
    
    else if (threesCounter >= 1 && threes) {
      System.out.println("You rolled threes");
      upperScore += threesCounter * 3;
      threes = false;
    }
    
    else if (twosCounter >= 1 && twos) {
      System.out.println("You rolled twos");
      upperScore += twosCounter * 2;
      twos = false;
    }
    
    else if (onesCounter >= 1 && aces) {
      System.out.println("You rolled Aces");
      upperScore += onesCounter;
      aces = false;
    }
    
    else if (chance) {
      System.out.println("You rolled a chance");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
      chance = false;
    }
    
    else {
      if (yahtzee) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Yahtzee'.");
        yahtzee = false;
      }
      else if (largeStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Large Straight'.");
        largeStraight = false;
      }
      else if (smallStraight) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Small Straight'.");
        smallStraight = false;
      }
      else if (fullHouse) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Full House'.");
        fullHouse = false;
      }
      else if (fourOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Four of a Kind'.");
        fourOfAKind = false;
      }
      else if (threeOfAKind) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Three of a Kind'.");
        threeOfAKind = false;
      }
      else if (sixes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Sixes'.");
        sixes = false;
      }
      else if (fives) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fives'.");
        fives = false;
      }
      else if (fours) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Fours'.");
        fours = false;
      }
      else if (threes) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Threes'.");
        threes = false;
      }
      else if (twos) {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Twos'.");
        twos = false;
      }
      else {
        System.out.println("You rolled something already taken and does not apply anywhere else. A score of 0 will be applied to 'Aces'.");
        aces = false;
      }
    }
    
    
    
    if (upperScore > 63 && upperScoreUsed) {
      System.out.println("Your upper score is greater than 63. You get a 35 point bonus");
      upperScore += 35;
      upperScoreUsed = false;
    }
    
    totalScore = upperScore + lowerScore;
    System.out.println("Upper score: " + upperScore + " Lower score: " + lowerScore + " Total Score: " + totalScore);
    
    onesCounter = 0;
    twosCounter = 0;
    threesCounter = 0;
    foursCounter = 0;
    fivesCounter = 0;
    sixesCounter = 0;
    }
  }
