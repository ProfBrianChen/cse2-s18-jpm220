/////////////////
//// Chapter 1 Question 4
///

public class Ch1Q4 {
  public static void main(String[] args) {
    int A = 1;
    int B = 2;
    int C = 3;
    int D = 4;
    System.out.println("a     a^2   a^3");
    System.out.println(A + "     " + A*A + "     " + A*A*A);
    System.out.println(B + "     " + B*B + "     " + B*B*B);
    System.out.println(C + "     " + C*C + "     " + C*C*C);
    System.out.println(D + "     " + D*D + "    " + D*D*D);
  }
}