/////////////////////////////////
///CSE2 Cyclometer   2/2/18
///Jeanine Minnick  Section 211
///Bike trip counter, including time, wheel rotations, and distance
//
public class Cyclometer {
  //main method required for every java program
  public static void main(String[] args) {
    //our input data
    int secsTrip1 = 480; //amount of time for the first trip
    int secsTrip2 = 3220; //amount of time for the second trip
    int countsTrip1 = 1561; //rotations for the first trip
    int countsTrip2 = 9037; //rotations for the second trip
    
    //our intermediate variables and output data
    double wheelDiameter = 27.0, //wheel diameter
    PI = 3.14159, //value of pi
    feetPerMile = 5280, //feet in a mile
    inchesPerFoot = 12, //inches in a foot
    secondsPerMinute = 60; //seconds in a minute
    double distanceTrip1, distanceTrip2, totalDistance; //assign variable for Trip 1 distance,
                                                        //Trip 2 distance, and the total distance
    
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    
    //Trip 1 took 8.0 minutes and had 1561 counts.
    //Trip 2 took 53.666666666666664 minutes and had 9037 counts.
    //For these values, we are converting seconds into minutes and showing how many minutes were in each trip, 
    //and then documenting the number of wheel rotations in that time
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    //above gives distance in inches
    //(for each count, a rotation of the wheel travels
    //the diameter in inches times PI)
    
    distanceTrip1 /= inchesPerFoot * feetPerMile; //Gives distance in miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
    totalDistance = distanceTrip1 + distanceTrip2;
    
    //Print out the output data
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles");
    System.out.println("The total distance was " + totalDistance + " miles");
    
    
  } //end of main method
} //end of class