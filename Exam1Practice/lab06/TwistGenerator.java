///////////////////////////////////////
/// Jeanine Minnick Section 211
/// Card Generator Lab06 3/2/18
/// ask the user for a length in a loop, and output the pattern based on the input


import java.util.Scanner;

public class TwistGenerator {
  public static void main(String [] args)  {
    
    Scanner myScanner = new Scanner(System.in);
    int length = 0;
    boolean go = true;
    int looper = 0;
    
    while (go) {
      System.out.println("Enter a positive integer: ");
      boolean findOut = myScanner.hasNextInt();
      
      
      if (findOut) {
        length = myScanner.nextInt();
        if (length < 0) {
          System.out.println("Try again");
        }
        else {
          break;
        }
      }
      
      else {
        String junkWord = myScanner.next();
        System.out.println("Try again");
      }
      }
      
    for (looper = 0; looper < length; ++looper) {
      if (looper % 3 == 0) {
        System.out.print("\\");
      }
      
      else if (looper % 3 == 1) {
        System.out.print(" ");
      }
      
      else if (looper % 3 == 2) {
        System.out.print("/");
      }
      
       
    }
    
    System.out.println(" ");
    
    for (looper = 0; looper < length; ++looper) {
      if (looper % 3 == 0) {
        System.out.print(" ");
      }
      
      else if (looper % 3 == 1) {
        System.out.print("X");
      } 
      
      else if (looper % 3 == 2) {
        System.out.print(" ");
      }
      
      
    }
    
    System.out.println(" ");
    
    for (looper = 0; looper < length; ++looper) {
      if (looper % 3 == 0) {
        System.out.print("/");
      }
      
      else if (looper % 3 == 1) {
        System.out.print(" ");
      } 
      
      else if (looper % 3 == 2) {
        System.out.print("\\");
      }
      
      
    }
    
    System.out.println(" ");
    
    } 
    }
    
    
  


