/////////////////////////////
/// Jeanine Minnick  Lab09  4/13/18

public class ArrayCopy {
  public static void main(String[] args) {
    
    int[] array0 = new int[]{0, 1, 2, 3, 4, 5, 6, 7};;
    
    
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    
		inverter(array0);
		
    inverter2(array1);
    
		int[] array3 = inverter2(array2);
		
		print(array1);
    print(array3);
 
  }
  
  public static int[] copy(int[] copyarray) {
    int[] copyarray1 = new int[copyarray.length];
    for (int i = 0; i < copyarray.length; i++) {
      copyarray1[i] = copyarray[i];
    }
    return copyarray1;
  }
  
  public static void inverter(int[] invertarray) {
		int temp = 0;
    for (int i = 0; i < invertarray.length / 2; i++) {
      temp = invertarray[i];
			invertarray[i] = invertarray[invertarray.length - 1 - i];
			invertarray[invertarray.length - 1 - i] = temp;
    }
    print(invertarray);
  }
  
  public static int[] inverter2(int[] inv2array) {
    int[] inv2array2 = copy(inv2array);
    int[] tempswitch = new int[inv2array2.length];
    for (int i = inv2array2.length - 1; i >= 0; i--) {
      tempswitch[i] = inv2array2[7 - i];
    }
    inv2array2 = tempswitch;
    return inv2array2;
    
  }
  
  public static void print(int[] printer) {
    String out = "{";
	  for (int j = 0; j < printer.length; j++){
  	  if (j > 0){
    	  out += ", ";
  	  } 
  	  out += printer[j];
	  }
	  out += "} ";
    System.out.println(out);
   }
}