//////////////////////////////////
///// Jeanine Minnick   Section 211
/// CSE2 Yahtzee  2/16/18
/// Roll dice, determine which category the roll falls under, and calculate score

import java.util.Scanner; //allow for user input

public class Yahtzee {  //create class
  public static void main(String[] args) { //create main function
    
    int userNumbers = 0, diceRoll1 = 0, diceRoll2 = 0, diceRoll3 = 0, diceRoll4 = 0, diceRoll5 = 0; //create variables for each dice value and user input
    int upperScore = 0, lowerScore = 0, bonus = 0, totalScore = 0; //create variables for scoring
    int onesCounter = 0, twosCounter = 0, threesCounter = 0, foursCounter = 0, fivesCounter = 0, sixesCounter = 0; //create variables to count dice values
    
    
    Scanner myScanner = new Scanner(System.in); //start up user input function
    
    System.out.println("Do you want to generate random numbers or create your own? Enter '1' for random, enter another number to create your own:"); //ask user for random or to input their own numbers
    int startRandomOrCYO = myScanner.nextInt(); //assign varable to input
    
    
    if (startRandomOrCYO == 1) { //if random numbers were chosen, generate 5 random dice values
      diceRoll1 = (int)(Math.random() * 6) + 1;
      diceRoll2 = (int)(Math.random() * 6) + 1;
      diceRoll3 = (int)(Math.random() * 6) + 1;
      diceRoll4 = (int)(Math.random() * 6) + 1;
      diceRoll5 = (int)(Math.random() * 6) + 1;
      System.out.println("You rolled " + diceRoll1 + " " + diceRoll2 + " " + diceRoll3 + " " + diceRoll4 + " " + diceRoll5); //show user what dice they rolled
    }
    
    else { //if the user does not choose random, they will be asked to input their own numbers
      System.out.println("Enter a 5 digit number pertaining to the 5 numbers you want to roll (1-6):");
      userNumbers = myScanner.nextInt();
      diceRoll1 = (int)userNumbers / 10000; //pull out first number they entered
      if (diceRoll1 < 1 || diceRoll1 > 6) { //is the first value 1-6?
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0); //if not 1-6, quit program
      }
      userNumbers = userNumbers - (diceRoll1 * 10000); //take off the first dice value of user input numbers
      diceRoll2 = (int)userNumbers / 1000; //pull out second number they entered
      if (diceRoll2 < 1 || diceRoll2 > 6) { //is the second value 1-6?
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0); //if not 1-6, quit program
      }
      userNumbers = userNumbers - (diceRoll2 * 1000); //take off the second dice value of user input number
      diceRoll3 = (int)userNumbers / 100; //pull out third number they entered
      if (diceRoll3 < 1 || diceRoll3 > 6) { //is the third value they entered 1-6?
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0); //if not 1-6, quit program
      }
      userNumbers = userNumbers - (diceRoll3 * 100); //take off the third dice value of user input number
      diceRoll4 = (int)userNumbers / 10; //pull out fourth number they entered
      if (diceRoll4 < 1 || diceRoll4 > 6) { //is the fourth value they entered 1-6?
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0); //if not 1-6, quit program
      }
      userNumbers = userNumbers - (diceRoll4 * 10); //take off the fourth dice value of user input number
      diceRoll5 = (int)userNumbers; //pull out fifth number they entered
      if (diceRoll5 < 1 || diceRoll5 > 6) { //is the fifth value they entered 1-6?
        System.out.println("Invalid number(s). Must be a 5-digit number with values from 1-6");
        System.exit(0); //if not 1-6, quit program
      }
      }
    
    // what was the first dice rolled? add to corresponding counter
    if (diceRoll1 == 1) { 
      onesCounter += 1;
    }
    if (diceRoll1 == 2) {
      twosCounter += 1;
    }
    if (diceRoll1 == 3) {
      threesCounter += 1;
    }
    if (diceRoll1 == 4) {
      foursCounter += 1;
    }
    if (diceRoll1 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll1 == 6) {
      sixesCounter += 1;
    }
    
    // what was the second dice rolled? add to corresponding counter
    if (diceRoll2 == 1) {
      onesCounter += 1;
    }
    if (diceRoll2 == 2) {
      twosCounter += 1;
    }
    if (diceRoll2 == 3) {
      threesCounter += 1;
    }
    if (diceRoll2 == 4) {
      foursCounter += 1;
    }
    if (diceRoll2 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll2 == 6) {
      sixesCounter += 1;
    }
    
    // what was the third dice rolled? add to corresponding counter
    if (diceRoll3 == 1) {
      onesCounter += 1;
    }
    if (diceRoll3 == 2) {
      twosCounter += 1;
    }
    if (diceRoll3 == 3) {
      threesCounter += 1;
    }
    if (diceRoll3 == 4) {
      foursCounter += 1;
    }
    if (diceRoll3 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll3 == 6) {
      sixesCounter += 1;
    }
    
    // what was the fourth dice rolled? add to corresponding counter
    if (diceRoll4 == 1) {
      onesCounter += 1;
    }
    if (diceRoll4 == 2) {
      twosCounter += 1;
    }
    if (diceRoll4 == 3) {
      threesCounter += 1;
    }
    if (diceRoll4 == 4) {
      foursCounter += 1;
    }
    if (diceRoll4 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll4 == 6) {
      sixesCounter += 1;
    }
    
    // what was the fifth dice rolled? add to corresponding counter
    if (diceRoll5 == 1) {
      onesCounter += 1;
    }
    if (diceRoll5 == 2) {
      twosCounter += 1;
    }
    if (diceRoll5 == 3) {
      threesCounter += 1;
    }
    if (diceRoll5 == 4) {
      foursCounter += 1;
    }
    if (diceRoll5 == 5) {
      fivesCounter += 1;
    }
    if (diceRoll5 == 6) {
      sixesCounter += 1;
    }
  
    
    // did you roll a yahtzee? if so, state and add to score
    if (onesCounter == 5 || twosCounter == 5 || threesCounter == 5 || foursCounter == 5 || fivesCounter == 5 || sixesCounter == 5) {
      System.out.println("You rolled a Yahtzee!");
      lowerScore += 50;
    }
    
    // did you roll a large straight? if so, state and add to score
    else if ((onesCounter == 1 && twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1) || (twosCounter == 1 && threesCounter == 1 && foursCounter == 1 && fivesCounter == 1 && sixesCounter == 1)) {
      System.out.println("You rolled a large straight");
      lowerScore += 40;
    }
    
    // did you roll a small straight? if so, state and add to score
    else if ((onesCounter >= 1 && twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1) || (twosCounter >= 1 && threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1) || (threesCounter >= 1 && foursCounter >= 1 && fivesCounter >= 1 && sixesCounter >= 1)) {
      System.out.println("You rolled a small straight");
      lowerScore += 30;
    }
    
    // did you roll a full house? if so, state and add to score
    else if ((onesCounter == 2 || twosCounter == 2 || threesCounter == 2 || foursCounter == 2 || fivesCounter == 2 || sixesCounter == 2) && (onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a full house");
      lowerScore += 25;
    }
    
    // did you roll a 4 of a kind? if so, state and add to score
    else if ((onesCounter == 4 || twosCounter == 4 || threesCounter == 4 || foursCounter == 4 || fivesCounter == 4 || sixesCounter == 4)) {
      System.out.println("You rolled a 4 of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
    }
    
    // did you roll a 3 of a kind? if so, state and add to score
    else if ((onesCounter == 3 || twosCounter == 3 || threesCounter == 3 || foursCounter == 3 || fivesCounter == 3 || sixesCounter == 3)) {
      System.out.println("You rolled a three of a kind");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
    }
    
    // did you roll any sixes? if so, state and add to score
    else if (sixesCounter >= 1) {
      System.out.println("You rolled sixes");
      upperScore += sixesCounter * 6;
    }
    
    // did you roll any fives? if so, state and add to score
    else if (fivesCounter >= 1) {
      System.out.println("You rolled fives");
      upperScore += fivesCounter * 5;
    }
    
    // did you roll any fours? if so, state and add to score
    else if (foursCounter >= 1) {
      System.out.println("You rolled fours");
      upperScore += foursCounter * 4;
    }
    
    // did you roll any threes? if so, state and add to score
    else if (threesCounter >= 1) {
      System.out.println("You rolled threes");
      upperScore += threesCounter * 3;
    }
    
    // did you roll any twos? if so, state and add to score
    else if (twosCounter >= 1) {
      System.out.println("You rolled twos");
      upperScore += twosCounter * 2;
    }
    
    // did you roll any ones? if so, state and add to score
    else if (onesCounter >= 1) {
      System.out.println("You rolled Aces");
      upperScore += onesCounter;
    }
    
    // did you not roll any of the previous choices? if so, it is counted as a "chance" and added to score
    else {
      System.out.println("You rolled a chance");
      lowerScore = diceRoll1 + diceRoll2 + diceRoll3 + diceRoll4 + diceRoll5;
    }
    
   
    // is your upper score worthy of the bonus? if greater than or equal to 63
     if (upperScore >= 63) {
       System.out.println("Your upper score reached 63. You get a 35 point bonus");
       bonus += 35;
       upperScore += bonus;
    }
    
    totalScore = upperScore + lowerScore; //add up total score
    System.out.println("Upper score: " + upperScore + " Bonus: " + bonus + " Lower score: " + lowerScore + " Total Score: " + totalScore); //print score totals
    
  }
}
    
    