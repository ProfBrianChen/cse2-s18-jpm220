import java.io.*;
import java.util.*;

public class rowcolumnarray {
    
   public static void main (String[] args) {
     int N = (int)(Math.random() * 5);
     int M = (int)(Math.random() * 5);
     int X = (int)(Math.random() * 5);
     int Y = (int)(Math.random() * 5);
    
     
     int[][] A = new int[N][M];
     int[][] B = new int[N][M];
     int[][] C = new int[X][Y];
       
     System.out.println("Generating a matrix with width " + N + " and height " + M + ":");     
     A = increasingMatrix(N, M, true);
     printMatrix(A, true);
     System.out.println("Generating a matrix with width " + N + " and height " + M + ":");
     B = increasingMatrix(N, M, false);
     printMatrix(B, false);
     System.out.println("Generating a matrix with width " + X + " and height " + Y + ":");
     C = increasingMatrix(X, Y, true);
     printMatrix(C, true);
     
     System.out.println("Adding 2 matrices.");
     printMatrix(A, true);
     System.out.println("plus");
     printMatrix(B, true);
     
     addMatrix(A, true, B, false);
     
     System.out.println("Adding 2 matrices.");
     printMatrix(A, true);
     System.out.println("plus");
     printMatrix(C, true);
     
     addMatrix(A, true, C, true);
    
    }
  
    public static int[][] increasingMatrix(int width, int height, boolean format) {
     
        int[][] A = new int[width][height];
        
        if (A[0].length == 0 | A.length == 0) {
          return null;
        }
     
        if (format) {
          int count = 1;
          for (int i = 0; i < A.length; i++)
            for (int j = 0; j < A[0].length; j++) {
                A[i][j] = count;
                count++;
            } 
        }
        
     
        else {
            int count = 1;
            for (int i = 0; i < A[0].length; i++)
              for (int j = 0; j < A.length; j++) {
                A[j][i] = count;
                count++;
            }
          }
          
        
        
        return A;
    }
  
  public static void printMatrix(int[][] array, boolean format) {
    if (array[0].length == 0 | array.length == 0) {
      System.out.println("The array is empty");
      return;
    }
    for (int[] i : array) {
      System.out.println(Arrays.toString(i));
    }
    return;
    } 
  
  
  public static int[][] translate(int[][] array) {
    System.out.println("Translating column major to row major input.");
    int h = array[0].length;
    int w = array.length;
    int[][] D = new int[1][w*h];
    
    if (array[0].length == 1 || array.length == 1) {
      return array;
    }
    
    int counter = 0;
    for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                D[0][counter] = array[j][i];
              counter++;
            }
    }
    
    
    
    int[][] array2 = new int[w][h];
    int counter1 = 0;
    for (int i = 0; i < w; i++) {
      for (int j = 0; j < h; j++) {
        array2[i][j] = D[0][counter1];
        counter++;
      }
    }
    
    return array2;
  }
     
    
  public static int[][] addMatrix(int[][] a, boolean formata, int[][] b, boolean formatb) {
    int[][] D = new int[a.length][a[0].length];
    
    if (a.length == 0 && a[0].length == 0) {
      System.out.println("The array is empty");
      return null;
    }
    
    if (!formata) {
      a = translate(a);
    }
    
    if (!formatb) {
      b = translate(b);
    }
      
    if (a.length == b.length && a[0].length == b[0].length) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                D[i][j] = a[i][j] + b[i][j];
            }
        }
      }
    
    else {
      System.out.println("Unable to add input matrices");
      return null;
    }
    
    System.out.println("output:");
    printMatrix(D, true);
    return D;
    }
  
}

   