/////////////////////////////////
///CSE2 Pyramid   2/10/18
///Jeanine Minnick  Section 211
///Ask for pyramid dimensions and output volume
//
//
import java.util.Scanner;

public class Pyramid {
  // main method required for every java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    System.out.print("The square side of the pyramid is (input length): "); // ask user for side length input
    double lengthPyr = myScanner.nextDouble(); // assign input value
    System.out.print("The height of the pyramid is (input height): "); // ask user for height input
    double heightPyr = myScanner.nextDouble(); // assign input value
    
    double volumePyr = lengthPyr * lengthPyr * heightPyr / 3; // compute volume using input values using lwh/3 where l=w
    
    System.out.println("The volume inside the pyramid is: " + volumePyr); // print out volume result
    
  } // end of main method
} // end of class