//////////////////////////
/// Jeanine Minnick  Lab06
/// Encrypted X   3/8/18
/// use loops to make a group of printouts that hide an X

import java.util.Scanner;

public class encrypted_x {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    
    boolean looper = true; 
    int input = 0;
    
    while (looper) {
      System.out.println("Enter an integer between 1-100: ");
      boolean question = myScanner.hasNextInt();
      
      if (question) {
        int user = myScanner.nextInt();
        if (user < 1 || user > 101) {
          System.out.println("Try again, make sure you are entering an integer 1-100");
        }
        else {
          looper = false;
          input = user;
          break;
        }
        
      }
      else {
       String junkWord = myScanner.next();
       System.out.println("Try again, make sure you are entering an integer 1-100");
      }
    
      
    
    }
    
    
    for (int i = 0; i < input; i++) {
      System.out.println();
      for(int j = 0; j < input; j++) {
        if (i == j || i == input - (j+1)) {
          System.out.print(" ");
        }
        else {
         System.out.print("*"); 
        }
      }
    }
}
}
