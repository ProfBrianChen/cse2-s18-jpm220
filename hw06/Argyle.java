//////////////////////////
/// Jeanine Minnick  hw06
/// Argyle   3/15/18
/// ask user for size and pattern input and create an argyle pattern

import java.util.Scanner;

public class Argyle {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    
    //declare variables for while loops
    boolean widthBool = true;
    boolean heightBool = true;
    boolean diamondWidthBool = true;
    boolean stripeWidthBool = true;
    
    // declare variables for input
    int width = 0;
    int height = 0;
    int diamondWidth = 0;
    int stripeWidth = 0;
    String firstChar = "";
    String secondChar = "";
    String thirdChar = "";
    
      
      
    while (widthBool) { //while loop for first question
      System.out.println("Please enter a positive integer for the width of Viewing window in characters: "); //ask user for entry
      boolean firstQ = myScanner.hasNextInt();
      
      if (firstQ) { //if the input is correct:
        width = myScanner.nextInt();
        break;
      }
      
      else { //if input is incorrect:
        String junkWord = myScanner.next();
        System.out.println("Try again, make sure you are entering an integer");
      }
    }
    
    
    while (heightBool) { //while loop for second question
      System.out.println("Please enter a positive integer for the height of Viewing window in characters: "); //ask user for entry
      boolean secondQ = myScanner.hasNextInt();
      
      if (secondQ) { //if the input is correct:
        width = myScanner.nextInt();
        break;
      }
      
      else { //if input is incorrect:
        String junkWord = myScanner.next();
        System.out.println("Try again, make sure you are entering an integer");
      }
    }
    
    
    while (diamondWidthBool) { //while loop for third question
      System.out.println("Please enter a positive integer for the width of the argyle diamonds: "); //ask user for entry
      boolean thirdQ = myScanner.hasNextInt();
      
      if (thirdQ) { //if the input is correct:
        diamondWidth = myScanner.nextInt();
        break;
      }
      
      else { //if input is incorrect:
        String junkWord = myScanner.next();
        System.out.println("Try again, make sure you are entering an integer");
      }
    }
    
    
    while (stripeWidthBool) { //while loop for fourth question
      System.out.println("Please enter a positive odd integer for the width of the argyle center stripe: "); //ask user for entry
      boolean fourthQ = myScanner.hasNextInt();
      
      if (fourthQ) { //if the input is correct:
        stripeWidth = myScanner.nextInt();
        int isOdd = stripeWidth % 2; //if the int is even, try again
        if (isOdd == 0) {
          System.out.println("Try again, make sure you are entering an integer");
        }
          else { //if int is odd, keep going
            break;
          }
      }
      
      else { //if input is incorrect:
        String junkWord = myScanner.next();
        System.out.println("Try again, make sure you are entering an integer");
      }
    }
    
    System.out.println("Please enter a first character for the pattern fill: "); //ask for the first character
    firstChar = myScanner.next();
    
    System.out.println("Please enter a second character for the pattern fill: "); //ask for the second character
    secondChar = myScanner.next();
    
    System.out.println("Please enter a third character for the stripe fill: "); //ask for the third character
    thirdChar = myScanner.next();
    
    int var = stripeWidth / 2; //a variable for distance from center of stripe
    
    for (int i = 0; i < diamondWidth; i++) { //loop for height
      System.out.println();
      for (int j = 0; j < 2 * diamondWidth; j++) { //loop for width
        for (int k = i - var; k <= i + var; k++) { //loop for stripe
            if (j == k && j < diamondWidth || j == 2 * diamondWidth - k - 1) { //print the stripe
              System.out.print(thirdChar);
              j++;
          
        }
        }
        if (j < diamondWidth - i && j < diamondWidth || j > diamondWidth + i - 1 && j < 2 * diamondWidth) { //print the diamond
          System.out.print(firstChar);
        }
        else { //fill in the rest
          System.out.print(secondChar);
        }
      }
    }
  }
} 

