///////////////////////////////////
//// Jeanine Minnick  HW06   3/2/18
//// Section 211 
/// Ask user for course information and make sure it matches the correct type, and print the output

import java.util.Scanner;

public class Hw06 {
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    
    int courseNum = 0; //create initial course number value
    String deptName = " "; //create initial department name value
    int meetsPerWeek = 0; //create inital days a week value
    String startTime = "00:00"; //create inital start time value (part 1)
    String startTime2 = " "; //create inital start time value (part 2)
    String instrName = " "; //create initial instructor name value (part 1)
    String instrName2 = " "; //create initial instructor name value (part 1)
    int numStudents = 0; //create initial enrollment value
    
    //create initial boolean values for each while loop to run
    boolean courseNumBool = true;
    boolean deptNameBool = true;
    boolean meetsPerWeekBool = true;
    boolean startTimeBool = true;
    boolean instrNameBool = true;
    boolean numStudentsBool = true;
    
    while (courseNumBool) { //while loop for first question
      System.out.println("Enter the course number: "); //ask user for entry
      boolean firstQ = myScanner.hasNextInt();
      
      if (firstQ) { //if the input is correct:
        courseNum = myScanner.nextInt();
        break;
      }
      
      else { //if input is incorrect:
        String junkWord = myScanner.next();
        System.out.println("Try again, make sure you are entering an integer");
      }
    }
    
    while (deptNameBool) { //while loop for second question
      System.out.println("Enter the department name: "); //ask user for entry
      boolean secondQ = myScanner.hasNext();
   
      if (secondQ) { //if the input is correct:
        deptName = myScanner.next();
        break;
      }
      
      else { //if input is incorrect:
        String junkWord = myScanner.next();
        System.out.println("Try again, make sure you are entering a string");
      }
    }
    
    while (meetsPerWeekBool) { //while loop for third question
      System.out.println("Enter the number of times per week the class meets: "); //ask user for entry
      boolean thirdQ = myScanner.hasNextInt();
      
      if (thirdQ) { //if the input is correct:
        meetsPerWeek = myScanner.nextInt();
        break;
      }
      
      else { //if input is incorrect:
        String junkWord = myScanner.next();
        System.out.println("Try again, make sure you are entering a integer");
      }
    }
    
    while (startTimeBool) { //while loop for fourth question
      System.out.println("Enter the start time (00:00 AM/PM): "); //ask user for entry
      boolean fourthQ = myScanner.hasNext();
      
      if (fourthQ) { //if the input is correct:
        startTime = myScanner.next();
        startTime2 = myScanner.next();
        break;
      }
      
      else { //if input is incorrect:
        String junkWord = myScanner.next();
        System.out.println("Try again, make sure you are entering a string");
      }
    }
    
    while (instrNameBool) { //while loop for fifth question
      System.out.println("Enter the instructor name: "); //ask user for entry
      boolean fifthQ = myScanner.hasNext();
      
      if (fifthQ) { //if the input is correct:
        instrName = myScanner.next();
        instrName2 = myScanner.next();
        break;
      }
      
      else { //if input is incorrect:
        String junkWord = myScanner.next();
        System.out.println("Try again, make sure you are entering a string");
      }
    }
    
    while (numStudentsBool) { //while loop for sixth question
      System.out.println("Enter the number of students: "); //ask user for entry
      boolean sixthQ = myScanner.hasNextInt();
      
      if (sixthQ) { //if the input is correct:
        numStudents = myScanner.nextInt();
        break;
      }
      
      else { //if input is incorrect:
        String junkWord = myScanner.next();
        System.out.println("Try again, make sure you are entering an integer");
      }
    }
    
    //print out total input from user
    System.out.println("You are taking: CRN: " + courseNum + " in the " + deptName + " department, which meets " + 
                       meetsPerWeek + " times per week at " + startTime + " " + startTime2 + " with " + instrName + 
                       " " + instrName2 + " and " + numStudents + " total students.");
    
  }
}