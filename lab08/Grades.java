/////////////////////////
/// Jeanine Minnick   Section 211 
/// lab08 Array Grades
/// create an array and ask the user for name inputs, and give random grades

import java.util.Scanner;

public class Grades {
  
  public static void main(String[] args) {
  int arraySize = (int)(5 + (Math.random() * 6));
    
  String[] StudentNames;
  StudentNames = new String[arraySize];
    
  Scanner myScanner = new Scanner(System.in);
  System.out.println("Enter " + arraySize + " student names: ");
    
    for (int i = 0; i < arraySize; i++) {
      StudentNames[i] = myScanner.nextLine();
    }
    
  int[] StudentGrades;
  StudentGrades = new int[arraySize];
    
    for (int j = 0; j < arraySize; j++) {
      StudentGrades[j] = (int)(Math.random() * 101);
    }
    
   System.out.println("Here are the midterm grades of the 5 students above:");
    
    for (int k = 0; k < arraySize; k++) {
      System.out.println(StudentNames[k] + ": " + StudentGrades[k]);
    }
  }
}