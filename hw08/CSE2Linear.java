/////////////////////////////
/// Jeanine Minnick Section 211
/// hw08 CSE2Linear
/// write a program that asks for grades, makes sure there is the correct input, and searches for a value in the list

import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class CSE2Linear {
  public static void main(String[] args) {
    
    int[] StudentGrades; //initalize array
    StudentGrades = new int[15]; //assign array size
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Enter 15 acsending ints for final grades in CSE2: "); //ask user for 15 int inputs
    
    int j = 0;
    do { //this is for the 0th value in the array because i-1 does not work in this case
      boolean Q = myScanner.hasNextInt();
      if (Q) { //continue if it is an int
        StudentGrades[0] = myScanner.nextInt();
  
      if (StudentGrades[0] < 101 && StudentGrades[0] > -1) { //continue if in the range 0-100
        j++;
      }
        else {
          System.out.println("Not a valid input. Try again.");
        }
      }
      else {
        System.out.println("Not an int. Try again.");
      }
    }
    while (j == 0); //condition for loop continuation
    
    for (int i = 1; i < 15; i++) { //loop for the rest of the ints in the array
      boolean Q = myScanner.hasNextInt();
      if (Q) { //continue if it is an int
        StudentGrades[i] = myScanner.nextInt();
  
      if (StudentGrades[i] > StudentGrades[i-1] && StudentGrades[i] < 101 && StudentGrades[i] > -1) { //continue if in the range 0-100 and new int is bigger than the last
        continue;
      }
        else {
          System.out.println("Not a valid input. Try again.");
          i--; //go back to correct array spot to fill in again
        }
      }
      else {
        System.out.println("Not an int. Try again.");
        i--; //go back to correct array spot to fill in again
      }
      
    }
    
    System.out.print("Enter a grade to search for: ");
    int search = myScanner.nextInt(); //search value
    
    //creation of variables for methods
    int lowerValue = 0;
    int upperValue = 14;
    int iterations = 1;
    int result = -1;
    BinarySearch(StudentGrades, lowerValue, upperValue, search, result, iterations); //call binary search method for array

    System.out.println("Scrambled:");
    Scramble(StudentGrades); //call for the scramble the array and print it out
    
    

  }  
    
    public static void LinearSearch(int[] list) { //linear search method
      Scanner myScanner = new Scanner(System.in);
      System.out.println("Enter a grade to search for: ");
      int user = myScanner.nextInt();
      for (int i = 0; i < 15; i++) { //go through every spot and search for the match
        if (user == list[i]) { //true if a value in array matches user input
          System.out.println(user + " was found in the list");
          return;
        }
      }
        System.out.println(user + " was not found in the list"); //if no matches
    }
    
    public static void BinarySearch(int temp[], int left, int right, int x, int res, int iter) { //method for binary search
        if (right >= left) { //continue if the last value is higher than the first
            int mid = (left + right)/2; //create a medium value

            if (temp[mid] == x) { //found the match if true
              res = x;
              System.out.println(x + " was found in the list with " + iter + " iterations");
              return;
            }
               
            else if (temp[mid] > x) { //go thorugh process again if user input is on the lower half of the array
              iter++;
              BinarySearch(temp, left, mid-1, x, res, iter);
              
            }
          
            else { //go thorugh process again if user input is on the upper half of the array
              iter++;   
              BinarySearch(temp, mid+1, right, x, res, iter);
            }
            
          
          
        }
          return;
    }
  
  public static void Scramble(int[] temp) { //scramble method
    Random rand = new Random(); //create random
    int randomNum = rand.nextInt(14); //random number 0-14
    for (int i = 0; i < 15; i++) { //loop for going through all values in array
      int j = rand.nextInt(i + 1); //random number 1-15
      //switch values
      int z = temp[j]; 
      temp[j] = temp[i];
      temp[i] = z;
    }
    System.out.println(Arrays.toString(temp)); //print out the array
    LinearSearch(temp); //call method for linear search
  }

}