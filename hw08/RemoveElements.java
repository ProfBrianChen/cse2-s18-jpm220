//////////////////////////
/// Jeanine Minnick Section 211
/// hw08 RemoveElements 4/10/18
/// fill in remaining methods to create a random array, 
/// then delete one element from the array, then delete all list matching the input from the array

import java.util.Scanner;
import java.util.Random;

public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
    
  int num[] = new int[10]; //create array
  int newArray1[]; //create array without defined size
  int newArray2[]; //create array without defined size
  int index,target; //create variables for user input
	String answer = "";
	
  do { 
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput(); //call randomInput method
  	String out = "The original array is:";
  	out += listArray(num); //add together both strings for printing
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt(); //assign user input to variable index
  	newArray1 = delete(num,index); //call delete method
  	String out1 = "The output array is ";
  	out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
    System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2 = "The output array is ";
  	out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer = scan.next();
    
	} 
    while (answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out = "{";
	for (int j = 0; j < num.length; j++){
  	if (j > 0){
    	out += ", ";
  	}
  	out += num[j];
	}
	out += "} ";
	return out;
  }
  
  public static int[] randomInput() { //method for creating original array with random variables 0-9
    int tempArray[] = new int[10]; //create temporary array
    Random rand = new Random();
    for (int i = 0; i < 10; i++) {
      tempArray[i] = rand.nextInt(10); //assign random variables 0-9 to each position in the array
    }
    
    return tempArray;
  }
  
  public static int[] delete(int list[], int pos) { //method for taking out one of the variables in the array in the position the user chose
    int list2[] = new int[9]; //create temporary array
    if (pos < 0 || pos > 9) {
      System.out.println("The index is not valid.");
      return list;
    }
    for (int i = 0; i < pos; i++){
      list2[i] = list[i]; //assign same values to spots up until the position the user chose
    }
    for(int i = pos; i < list.length - 1; i++){
      list2[i] = list[i + 1]; //skip over the position the user chose and copy over the rest of the values to the new array
      }
    
    return list2;
  }
  
  public static int[] remove(int list[], int target) { //method to take out all values matching user input
    int count = 0; // loop over array to count number of target values
    for (int i = 0; i < list.length; i++) { //loop to count the number of occurrences
      if (list[i] == target) { 
        count++;
        if (count == 1) { //state that the input has been found
          System.out.println("Element " + target + " has been found");
        }
      } 
    }
    if (count == 0) { //print original array if no occurrences and state that there are none
      System.out.println("Element " + target + " was not found");
      return list; 
    }
    int[] result = new int[list.length - count];  //create an array the correct size without the target values
    int spot = 0;
    for (int i = 0; i < list.length; i++) { 
      if (list[i] != target) { //copy over values if not target value 
        result[spot] = list[i]; 
        spot++; //counter for occurrences to print in correct place
      } 
    }
    
    
    return result;


}
}

