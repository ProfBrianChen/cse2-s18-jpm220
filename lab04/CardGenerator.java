///////////////////////////////////////
/// Jeanine Minnick Section 211
/// Card Generator Lab04 2/16/18
/// Generate a random card out of a 52 card deck


public class CardGenerator {
  public static void main(String[] args) {
    
    int cardValue = (int)(Math.random() * 52) + 1; //generate random card number 1-52
    
    int cardNum; //create card number variable
    String cardSuit; //create card suit variable
    
    if (cardValue <= 13) { //if statement for diamonds
      cardSuit = "diamonds";
      }
      else if (cardValue >= 14 && cardValue <= 26) { //if statement for clubs
         cardSuit = "clubs";
      }
      else if (cardValue >= 27 && cardValue <= 39){ //if statement for hearts
         cardSuit = "hearts";
      }
      else { //if statement for spades
         cardSuit = "spades";
      }
  
    
    if (cardValue % 13 == 1) { //if statement that determines what card number and prints the value of the card
      String cardRoyal = "Ace";
      System.out.println("You picked the " + cardRoyal + " of " + cardSuit);
      System.exit(0);
      }
      else if (cardValue % 13 == 2) {
        cardNum = 2;
        System.out.println("You picked the " + cardNum + " of " + cardSuit);
        System.exit(0);
      }
      else if (cardValue % 13 == 3) {
        cardNum = 3;
        System.out.println("You picked the " + cardNum + " of " + cardSuit);
        System.exit(0);
      }
      else if (cardValue % 13 == 4) {
        cardNum = 4;
        System.out.println("You picked the " + cardNum + " of " + cardSuit);
        System.exit(0);
      }
      else if (cardValue % 13 == 5) {
        cardNum = 5;
        System.out.println("You picked the " + cardNum + " of " + cardSuit);
        System.exit(0);
      }
      else if (cardValue % 13 == 6) {
        cardNum = 6;
        System.out.println("You picked the " + cardNum + " of " + cardSuit);
        System.exit(0);
      }
      else if (cardValue % 13 == 7) {
        cardNum = 7;
        System.out.println("You picked the " + cardNum + " of " + cardSuit);
        System.exit(0);
      }
      else if (cardValue % 13 == 8) {
        cardNum = 8;
        System.out.println("You picked the " + cardNum + " of " + cardSuit);
        System.exit(0);
      }
      else if (cardValue % 13 == 9) {
        cardNum = 9;
        System.out.println("You picked the " + cardNum + " of " + cardSuit);
        System.exit(0);
      }
      else if (cardValue % 13 == 10) {
        cardNum = 10;
        System.out.println("You picked the " + cardNum + " of " + cardSuit);
        System.exit(0);
      }
      else if (cardValue % 13 == 11) {
        String cardRoyal = "Jack";
        System.out.println("You picked the " + cardRoyal + " of " + cardSuit);
        System.exit(0);
      }
      else if (cardValue % 13 == 12) {
        String cardRoyal = "Queen";
        System.out.println("You picked the " + cardRoyal + " of " + cardSuit);
        System.exit(0);
      }
      else if (cardValue % 13 == 0) {
        String cardRoyal = "King";
        System.out.println("You picked the " + cardRoyal + " of " + cardSuit);
        System.exit(0);
      }
            
    }
    
  }