////////////////////////
/// Jeanine Minnick Section 211
/// hw07 Area 3/23/18
/// ask user for choice of 3 shapes and ask user for input to calculate the area of the shape


import java.util.Scanner;

public class Area { //main class
  public static void main(String[] args){ //main method
    
    
    Scanner myScanner = new Scanner(System.in); //assign scanner
    
    boolean looper = true; //variable for while loop to run
    
    while (looper) { //continuous loop for user to put in correct input
      String input = " ";
      String user = Shape(input); //call Shape method to initiate the user's shape choice
    
      if (user.equals("rectangle")) { //if user chooses rectangle, ask user for sides input
        System.out.println("Enter values for the following variables");
        System.out.print("Base: ");
        double bottom = myScanner.nextDouble();
        System.out.print("Height: ");
        double side = myScanner.nextDouble();
        
        double rectangle = rectangleArea(bottom, side); //calculate the area by calling rectangleArea method
        
        System.out.println("The area of the rectangle is " + rectangle); //give user area output
        looper = false;
      }
      
      else if (user.equals("triangle")) { //if user chooses triangle, ask user for sides input
        System.out.println("Enter values for the following variables");
        System.out.print("Height: ");
        double tall = myScanner.nextInt();
        System.out.print("Base Width: ");
        double bottom = myScanner.nextInt();
        
        double triangle = triangleArea(tall, bottom); //calculate the area by calling triangleArea method
        
        System.out.println("The area of the isoceles triangle is " + triangle); //give user area output
        looper = false;
      }
      
      else if (user.equals("circle")) { //if user chooses circle, ask user for sides input
        System.out.println("Enter values for the following variables");
        System.out.print("Radius: ");
        double radius = myScanner.nextDouble();
        
        double circle = circleArea(radius); //calculate the area by calling circleArea method
        
        System.out.println("The area of the circle is " + circle); //give user area output
        looper = false;
      }
          
      else { //if user does not type in a correct shape, ask again
        System.out.println("Incorrect input. Make sure your shape matches one of the choices");
      }
    }
    
    
    
  
  }
  
  public static String Shape(String userChoice) { //shape method to ask for initial shape input
      Scanner myScanner = new Scanner(System.in);
      System.out.println("circle");
      System.out.println("rectangle");
      System.out.println("triangle");
      System.out.println("Choose from one of these shapes (enter shape name CASE SENSITIVE): ");
      userChoice = myScanner.next();
      return userChoice;
    } //end of shape method
  
  public static double circleArea(double radius) {  //circleArea method to calculate circle area   
    double area = Math.PI * (radius * radius);
    return area;
  } //end of circleArea method
  
   public static double rectangleArea(double base, double width) { //rectangleArea method to calculate rectangle area
    double area = base * width;
    return area;
  } //end of rectangleArea method
  
  public static double triangleArea(double height, double base) { //triangleArea method to calcluate triangle area
    double area = base * height * 0.5;
    return area;
  }//end of triangleArea method
 
} //end of class







