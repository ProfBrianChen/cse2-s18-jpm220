///////////////////////
/// Jeanine Minnick  Section 211
/// hw07  StringAnalysis  3/24/18
/// ask the user for a string and ask for the number of characters to evaluate, then determine how many are letters


import java.util.Scanner;


public class StringAnalysis { //main class
  public static void main(String[] args) { //main method
    
   Scanner myScanner = new Scanner(System.in); //initialize scanner
    
   boolean looper = true; //variable to continue while loop
   String input = ""; //create input variable for user input
    
   while (looper) { //loop for asking user for the string
     System.out.println("Enter a string of any type: ");
     boolean stringerQ = myScanner.hasNext(); //accept user input
     
     if (stringerQ) { //finish loop if string is OK
       input = myScanner.next();
       looper = false;
       break;
     }
     
     else { //ask user for input again if string input is bad
       System.out.println("Incorrect input. Try again.");
       String junkword = myScanner.next();
     }
   }
    
    boolean looper2 = true; //variable for second while loop
    int integer = 0; //create variable for user input of number of characters to evaluate
    
    while (looper2) { //loop for asking user for number of characters
    System.out.println("Enter the number of characters in the string you want to evaluate: ");
    boolean isCountInt = myScanner.hasNextInt(); //accept if input is of type int
     
     if (isCountInt) { //finish loop if not an int
       integer = myScanner.nextInt();
       looper2 = false;
       break;
     }
     
     else { //try again if not an int
       System.out.println("Incorrect input. Try again, make sure you are entering an integer.");
       String junkword = myScanner.next();
     }
   }
    
    IsLetter(input, integer); //call IsLetter method
  }
  
  public static int IsLetter(String counter, int numCount) { //method to carry out counting function
    
    int numChar = 0;
    
    if (counter.length() < numCount - 1) { //if user number is over the number of characters in the string, change user input to number of characters
      numCount = counter.length(); 
    }

    
    for (int i = 0; i <= numCount - 1; i++) { //loop to count the number of letters
      
        char j = counter.charAt(i); //variable for going through each character of string
      
       if (j >= 'a' && j <= 'z' || j >= 'A' && j <= 'Z') { //find character values that are letters
          numChar = numChar + 1; //add to total letter count
       }   
      }
    System.out.println("Your string has " + numChar + " letters");
    return numChar;
    }

  }


