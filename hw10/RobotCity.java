///////////////////////////////
/// Jeanine Minnick  Section 211
/// hw10 robotcity  4/23/18
/// create an array filled with populations and randomly choose locations to "invade" and then update it to see how the robots move east

import java.util.*;

public class RobotCity {
  public static void main(String[] args){
    //create random ints 10-15 for block size
    int NS = (int)(Math.random() * 5) + 10;
    int EW = (int)(Math.random() * 5) + 10;
    
    int[][] cityblock = new int[EW][NS]; //create the city block array
    
    System.out.println("BUILD");
    cityblock = buildCity(cityblock); //call buildCity method to enter the populations in the city
    
    display(cityblock); //call method to print the array
    
    int robots = (int)(Math.random() * 12); //create number of robots
    
    System.out.println();
    System.out.println("INVADE");
    cityblock = invade(cityblock, robots); //call method to invade the city
    
    System.out.println();
    display(cityblock); //print the new invaded city
    
    //loop 5 times to update the city as the robots move east
    for (int i = 1; i < 6; i++) {
      System.out.println();
      System.out.println("UPDATE " + i);
      cityblock = update(cityblock);
      display(cityblock);
    }
    
  }
  
  public static int[][] buildCity(int[][] block) { //method to fill the array with populations
    
    //for loops go through every spot in the array to create a random population
    for (int i = 0; i < block.length; i++) {
      for (int j = 0; j < block[0].length ; j++) {
        block[i][j] = (int)(Math.random() * 899) + 100;
      }
    }
    return block;
  }
  
  public static void display(int[][] blockpop) { //method to print out the array to see what it contains
    
    //for loops go through every spot in the array to print
    for (int i = 0; i < blockpop.length; i++) {
      for (int j = 0; j < blockpop[0].length; j++) {
          System.out.printf("%-6d", blockpop[i][j]); //printf for spacing purposes
    }
    System.out.println();
    }
    return;
  }
  
  public static int[][] invade(int[][] cityinvade, int k) { //method for randomly placing robots and turning the population negative
    for (int i = 0; i < k + 1; i++) {
      int q = (int)(Math.random() * cityinvade.length);
      int z = (int)(Math.random() * cityinvade[0].length);
      if (cityinvade[q][z] > 0) { //makes sure the robot doesn't go somewhere already occupied
        cityinvade[q][z] = -cityinvade[q][z];
      }
    }
  return cityinvade;
  }
  
  public static int[][] update(int[][] updatecity) { //method for moving the invaded spots east and leaving what is already negative alone
    
    //for loops go through every spot in the array to make negative what needs to be negative
    for (int i = 0; i < updatecity.length; i++) {
      for (int j = 0; j < updatecity[0].length; j++) {
          if (updatecity[i][j] < 0 && j < updatecity[0].length - 1 && updatecity[i][j + 1] > 0) { //makes sure the loop doesn't go out of bounds and the spot to it's east isn't already negative
            updatecity[i][j + 1] = -updatecity[i][j + 1];
            j++;
          }
    }
    
    }
    return updatecity;
  }
}